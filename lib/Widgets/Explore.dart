import 'package:flutter/material.dart';
import 'package:wakelock/wakelock.dart';
import 'Stores/StoresMap.dart';
import '../Classes/Coordinate.dart';
import '../Classes/SB_Settings.dart';
import '../Helpers/GeoLocationHelper.dart';

class Explore extends StatefulWidget
{
	
	Explore();
	
	@override
	_ExploreState	createState() => _ExploreState();
}

class _ExploreState extends State<Explore>
{
	Coordinate	location;
	StoresMapController		_ctrlMap;
	bool					_follow = false;
	
	@override
	void initState()
	{
		Wakelock.enable();
		this._getData();
		super.initState();
	}
	@override
	void dispose()
	{
		Wakelock.disable();
		GeoLocationHelper.stopFollowLocation();
		super.dispose();
	}
	void _getData() async
	{
		var l = Coordinate.fromMap( await SB_Settings.getObject('location'));
		this.setState( ()
		{
			this.location = l;
		});
		
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(
				title: InkWell(
					child: Container(
						padding: EdgeInsets.all(3),
						child: Row(
							mainAxisAlignment: MainAxisAlignment.start,
							children:[
								Icon(Icons.location_on_outlined, color: Colors.white, size: 17), 
								SizedBox(width: 8),
								Text('A menos de 20Km', style: TextStyle(fontSize: 15, color: Colors.white)),
								Icon(Icons.keyboard_arrow_down_outlined , color: Colors.white, size: 20), 
							]
						)
					),
					onTap: ()
					{
					}
				),
				actions: [
					IconButton(
						tooltip: 'Obtener mi ubicación actual',
						icon: Icon(Icons.location_searching_outlined ),
						onPressed: this._onGetLocation,
					),
					if( !this._follow )
						IconButton(
							tooltip: 'Iniciar el seguimiento de mi ubicación',
							icon: Icon(Icons.looks_outlined),
							onPressed: this._onFollowLocation,
						),
					if( this._follow )
						IconButton(
							tooltip: 'Parar el seguimiento de mi ubicación',
							icon: Icon(Icons.stop_circle_outlined),
							onPressed: this._onStopFollowLocation,
						),
				]
			),
			body: Container(
				child: Stack(
					fit: StackFit.expand,
					//alignment: AlignmentDirectional.center,
					children: [
						if( this.location != null )
							StoresMap(
								center: this.location, 
								onCreated: (ctrl)
								{
									this._ctrlMap = ctrl;
								}
							),
						if( this.location == null )
							CircularProgressIndicator(),
						
						Column(
							mainAxisAlignment: MainAxisAlignment.end,
							children: [
								Container(
									padding: EdgeInsets.all(10),
									child: Container(
										color: Colors.white,
										child: TextFormField(
											decoration: InputDecoration(
												filled: true,
												fillColor: Colors.white,
												icon: Icon(Icons.search, color: Colors.black),
												hintText: 'Nombre del comercio',
												contentPadding: EdgeInsets.all(2),
											)
										)
									)
								)
							]
						)
					]
				)
			)
		);
	}
	void _onGetLocation() async
	{
		try
		{
			var c = await GeoLocationHelper.getLocation();
			print(c.toMap());
			this._ctrlMap.setPosition( c );
			SB_Settings.saveObject('location', c.toMap());
			
		}
		catch(e)
		{
			print('GET LOCATION ERROR: $e');
		}
	}
	void _onFollowLocation() async
	{
		GeoLocationHelper.startFollowLocation(0.05, (location) 
		{
			this._ctrlMap.setPosition( location );
		});
		this.setState( () 
		{
			this._follow = true;
		});
	}
	void _onStopFollowLocation() async
	{
		GeoLocationHelper.stopFollowLocation();
		this.setState( () 
		{
			this._follow = false;
		});
	}
}
