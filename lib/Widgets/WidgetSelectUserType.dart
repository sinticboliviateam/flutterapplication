import 'package:flutter/material.dart';

import 'WidgetLogin.dart';
import 'WidgetUserHome.dart';

class WidgetSelectUserType extends StatelessWidget
{
	@override
	Widget build(BuildContext context)
	{
		var size = MediaQuery.of(context).size;
		
		return Scaffold(
			body: SafeArea(
				child: Container(
					color: Colors.black,
					child: Stack(
						fit: StackFit.expand,
						overflow: Overflow.visible,
						children: <Widget>[
							Column(
								crossAxisAlignment: CrossAxisAlignment.stretch,
								children: [
									Expanded(
										child: Opacity(
											opacity: 0.6,
											child: Image.asset('images/comida_2.jpg', fit: BoxFit.cover,),
										)
									),
									Expanded(
										child:  Opacity(
											opacity: 0.4,
											child: Image.asset('images/panaderia.jpg', fit: BoxFit.cover),
										)
									),
									Expanded(
										child:  Opacity(
											opacity: 0.5,
											child: Image.asset('images/supermercados.jpg', fit: BoxFit.cover),
										)
									),
								]
							),
							Positioned.fill(
								child: Column(
									mainAxisAlignment: MainAxisAlignment.center,
									children: [
										//SizedBox(height: size.height * 0.3),
										Text("Aprovechalo", style: TextStyle(color: Colors.white, fontSize: 20), textAlign: TextAlign.center,),
										SizedBox(height: 20),
										IntrinsicWidth(
											child: Column(
												crossAxisAlignment: CrossAxisAlignment.stretch,
												children: [
													Container(
														color: Color(0xfff75428),
														child: InkWell(
															child: Container(
																padding: EdgeInsets.all(15),
																child: Text("Soy Usuario", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,) 
															),
															onTap: ()
															{
																Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetLogin(type: "user")));
															},
														)
													),
													SizedBox(height: 20),
													Container(
														color: Theme.of(context).primaryColor,
														child: InkWell(
															child: Container(
																padding: EdgeInsets.all(15),
																child: Text("Soy Empresario", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,) 
															),
															onTap: ()
															{
																Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetLogin(type: "company")));
															},
														)
													),
													SizedBox(height: 20),
													Container(
														color: Colors.indigo,
														child: InkWell(
															child: Container(
																padding: EdgeInsets.all(15),
																child: Text("Soy Vistante", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,) 
															),
															onTap: ()
															{
																Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetUserHome()));
															},
														)
													),
												]
											),
										)
									]
								),
							)
						],
					)
				),
			)
		);
	}
}