import 'package:flutter/material.dart';
import '../../Services/ServiceContents.dart';
import '../../Classes/SB_Content.dart';

class WidgetAds extends StatefulWidget
{
	final double	height;
	final double	width;
	
	WidgetAds({this.width = 120, this.height = 120,});
	
	@override
	_WidgetAdsState	createState() => _WidgetAdsState();
}

class _WidgetAdsState extends State<WidgetAds>
{
	List<SB_Content>	ads = [];
	
	@override
	void initState()
	{
	}
	@override
	Widget build(BuildContext context)
	{
		return FutureBuilder(
			future: ServiceContents.getContents('ads', 6),
			builder: (ctx, snapshot)
			{
				if( !snapshot.hasData )
					return Center(child: CircularProgressIndicator());
				this.ads = [];
				//this.ads = snapshot.data.map<SB_Content>( (d) => SB_Content.fromJson(d) );
				snapshot.data.forEach( (d) => this.ads.add( SB_Content.fromJson(d) ) );
				
				return Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						SizedBox(height: 10),
						Container(
							padding: EdgeInsets.all(10),
							child: Text('Anuncios Destacados', style: TextStyle(fontWeight: FontWeight.bold)),
						),
						
						Container(
					
							height: this.widget.height,
							child: ListView.builder(
								scrollDirection: Axis.horizontal,
								itemCount: this.ads.length,
								itemBuilder: (lctx, int index)
								{
									return this._buildAds( this.ads[index] );
								}
							)
						)
					]
				);
			}
		);
	}
	Widget _buildAds(SB_Content ads)
	{
		return Card(
			elevation: 2,
			child: Container(
				width: this.widget.width,
				padding: EdgeInsets.all(8),
				child: InkWell(
					child: Image.network(ads.thumbnail_url, fit: BoxFit.cover),
					onTap:()
					{
					}
				)
			)
		);
	}
}
