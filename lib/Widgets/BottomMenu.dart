import 'package:flutter/material.dart';
import '../main.dart';
import 'WidgetUserHome.dart';
import 'Explore.dart';
import '../Classes/SB_Settings.dart';
import '../Classes/Coordinate.dart';
import 'Favorites/Favorites.dart';
import '../Services/ServiceUsers.dart';
import 'WidgetLogin.dart';

class BottomMenu extends StatefulWidget
{
	final	int	itemIndex;
	
	BottomMenu({this.itemIndex = 0});
	
	@override
	_BottomMenuState	createState() => _BottomMenuState(currentIndex: this.itemIndex);
}

class _BottomMenuState extends State<BottomMenu>
{
	List<Map<String, dynamic>> menuItems = [
		{'icon': Icons.home_outlined, 'title': 'Inicio', 'key': 'home'},
		{'icon': Icons.location_on_outlined, 'title': 'Explorar', 'key': 'explore'},
		{'icon': Icons.favorite_border, 'title': 'Favoritos', 'key': 'favorites'},
	];
	int	currentIndex = 0;
	
	_BottomMenuState({this.currentIndex});
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return BottomNavigationBar(
			currentIndex: this.currentIndex,
			items: <BottomNavigationBarItem>[
				for(int i = 0; i < this.menuItems.length; i++)
					BottomNavigationBarItem(
						icon: this.menuItems[i]['icon'] != null ? Icon(this.menuItems[i]['icon']) : null,
						title: Text(this.menuItems[i]['title'])
						
					)
			],
			onTap: (int index) async
			{
				this.setState( () 
				{
					this.currentIndex = index;
				});
				String key = this.menuItems[this.currentIndex]['key'];
				if( key == 'home' )
				{
					Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => WidgetUserHome()), (_) => true);
				}
				else if( key == 'explore' )
				{
					//var location = await SB_Settings.getObject('location');	
					Navigator.push(context, MaterialPageRoute(builder: (_) => Explore()));
				}
				else if( key == 'favorites' )
				{
					if( !await ServiceUsers.isLoggedIn() )
					{
						Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetLogin(type: 'user')));
						return;
					}
					Navigator.push(context, MaterialPageRoute(builder: (_) => Favorites()));
				}
			}
		);
	}
	
}
