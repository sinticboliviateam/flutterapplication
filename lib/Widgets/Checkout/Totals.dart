import 'package:flutter/material.dart';
import '../../Classes/MB_Cart.dart';

class Totals extends StatefulWidget
{
	@override
	_TotalsState createState() => _TotalsState();
}

class _TotalsState extends State<Totals>
{
	MB_Cart					_cart = MB_Cart();
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Card(
			child: Container(
				decoration: BoxDecoration(
					 borderRadius: BorderRadius.circular(10),
					 color: Colors.white,
					 /*
					 border: new Border.all(color: Colors.grey, width:1),
					 boxShadow: [
						BoxShadow(
							color: Colors.grey,
							offset: Offset(2, 2),
							blurRadius: 4,
						)
					 ]
					 */
				),
				padding: EdgeInsets.all(10),
				child: Column(
					children: [
						Row(
							children:[
								Expanded(
									child: Text(
										'Total del pedido',
										style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)
									)
								),
								Text(
									this._cart.getTotals().toStringAsFixed(2) + ' Bs.', 
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)
								),
							]
						),
						Row(
							children:[
								Expanded(
									child: Text(
										'Costo delivery',
										style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)
									)
								),
								Text(
									this._cart.getDeliveryCost().toStringAsFixed(2) + ' Bs.', 
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)
								),
							]
						),
						Row(
							children:[
								Expanded(
									child: Text(
										'Total a pagar',
										style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)
									)
								),
								Text(
									(this._cart.getTotals() + this._cart.getDeliveryCost()).toStringAsFixed(2)  + ' Bs.', 
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)
								),
							]
						),
					]
				)
			),
		);
	}
}