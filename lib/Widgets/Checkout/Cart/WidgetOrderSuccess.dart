import 'package:flutter/material.dart';
import '../../WidgetButton.dart';
//import '../../Users/WidgetOrders.dart';

class WidgetOrderSuccess extends StatelessWidget
{
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			body: SafeArea(
				child: Container(
					margin: EdgeInsets.only(right: 10, left: 10),
					child: Column(
						children: [
							SizedBox(height:50),
							Center(
								child: Container(
									width: 80,
									height: 80,
									decoration: BoxDecoration(
										color: Colors.green,
										borderRadius: BorderRadius.circular(50)
									),
									child: Icon(
										Icons.check,
										color: Colors.white,
										size: 50,
									),
								)
							),
							SizedBox(height:50),
							Center(
								child: Text('Su pedido fue recibido correctamente!!!', 
									textAlign: TextAlign.center,
									style: TextStyle(fontSize: 30, )
								)
							),
							SizedBox(height:50),
							Center(
								child: Text(
									'Nos pondremos en contacto contigo en un lapso de 24 horas para confirmar tu pedido.', 
									style: TextStyle(fontSize: 15),
									textAlign: TextAlign.center,
								)
							),
							SizedBox(height:50),
							WidgetButton(text: 'Seguir comprando', 
								callback: ()
								{
									Navigator.of(context).popUntil((route) => route.isFirst);
								}, 
								type: 'primary'
							),
							SizedBox(height:50),
							WidgetButton(text: 'Ver mis pedidos', 
								callback: ()
								{
									//Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetOrders()));
								}, 
								type: 'warning'
							),
						]	
					)
				)
			)
		);
	}
}
