import 'package:flutter/material.dart';
import 'dart:convert';
//import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:provider/provider.dart';
import '../../../Classes/MB_User.dart';
import '../../WidgetButton.dart';
import '../../../Classes/SB_Settings.dart';
import '../../../Classes/SB_Geo.dart';
import '../../../Classes/MB_Cart.dart';
import '../../../Classes/MB_Order.dart';
import '../../../Classes/SB_Globals.dart';
import '../../../Services/ServiceOrder.dart';
import '../../../Services/ServiceUsers.dart';
import '../../WidgetLoading.dart';
import 'WidgetOrderSuccess.dart';
import '../../../Providers/CartProvider.dart';
import '../Steps.dart';
import '../UserData.dart';
import '../Totals.dart';

class WidgetCheckout extends StatefulWidget
{
	@override
	WidgetCheckoutState		createState() => WidgetCheckoutState();
}
class WidgetCheckoutState extends State<WidgetCheckout>
{
	MB_Cart					_cart = MB_Cart();
	TextEditingController	_ctrlName;
	TextEditingController	_ctrlEmail;
	TextEditingController	_ctrlPhone;
	TextEditingController	_ctrlAddress;
	TextEditingController	_ctrlDateTime;
	TextEditingController	_ctrlPaymentMethod	= TextEditingController();
	String					_paymentMethod;
	final					_formKey = GlobalKey<FormState>(); 
	List					_zonas = [];//<DropdownMenuItem<String>>[];
	List					_barrios = [];
	String					zonaId;
	String					barrioId;
	Map						gdata;
	SB_User					user;
	DateFormat				dateFormat = DateFormat('dd-MM-yyyy HH:mm');
	MB_Order				order;
	
	void _buildOrder()
	{
		this.order = new MB_Order.fromCart(this._cart);
	
		if( this.user != null )
		{
			order.customer_id 	= 0;
			order.user_id		= this.user.user_id;
		}
		if( this.gdata != null )
		{
			order.meta['_lat'] = this.gdata['latitude'];
			order.meta['_lng'] = this.gdata['longitude'];
		}
		order.meta['_zona']				= this.zonaId.toString();
		order.meta['_barrio'] 			= this.barrioId.toString();
		order.meta['_shipping_address'] = this._ctrlAddress.text.trim();
		order.meta['_payment_method']	= this._paymentMethod;
		
		order.customer_phone			= this._ctrlPhone.text.trim();
		order.delivery_date				= this._ctrlDateTime.text.trim();
		order.shipping_cost				= this._cart.getDeliveryCost();
	}
	void _sendOrder(context)
	{
		
		if( !this._formKey.currentState.validate() )
			return;
		/*
		var alert = AlertDialog(
			title: Text("Pedido exitoso"),
			content: Text("Su fue recibido correctamente, nos pondremos en contacto con usted en un laptop de 4 horas para poder confirmarlo."),
			actions: <Widget>[
				FlatButton(
					child: Text("Ok"), 
					onPressed: ()
					{
						//Navigator.of(context).pop();
						Navigator.of(context).popUntil((route) => route.isFirst);
						
					}
				)
			]
		);
		*/
		
		this._buildOrder();		
		final GlobalKey<State> _key = GlobalKey<State>();
		WidgetLoading.show(context, _key, 'Procesando su pedido...');
		ServiceOrder.send(this.order)
			.then( (newOrder) 
			{
				WidgetLoading.hide(_key);
				//showDialog(context: context, builder: (BuildContext ctx){return alert;});
				//##clear shopping cart
				this._cart.clear();
				Provider.of<CartProvider>(context, listen: false).quantity = 0;
				Navigator.push(context, MaterialPageRoute(builder: (ctx) => WidgetOrderSuccess()));
			})
			.catchError( (e) 
			{
				WidgetLoading.hide(_key);
				print('ORDER ERROR');
				print(e);
			});
		
	}
	void _showPaymentMethods()
	{
		showModalBottomSheet(context: context, builder: (ctx) 
		{
			return SafeArea(
				child: StatefulBuilder(
					builder: (_ctx, setter)
					{
						return Container(
							//height: 520,
							color: Color(0xFF737373),
							child: Container(
								//height: 450,
								padding: EdgeInsets.all(10),
								decoration: BoxDecoration(
									color: Theme.of(context).canvasColor,
									borderRadius: BorderRadius.only(
										topLeft: Radius.circular(10),
										topRight: Radius.circular(10),
									),
								),
								child: null
							)
						);
					},
				)
			);
		});
	}
	@override
	void initState()
	{
		super.initState();
		this._ctrlName 		= TextEditingController();
		this._ctrlEmail 	= TextEditingController();
		this._ctrlPhone 	= TextEditingController();
		this._ctrlAddress 	= TextEditingController();
		this._ctrlDateTime	= TextEditingController();
		ServiceUsers.getCurrentUser()
			.then( (user) 
			{
				if( user != null )
				{
					this.user = user;
					print(user.meta);
					this.setState(()
					{
						this._ctrlName.text = user.fullname;
						this._ctrlEmail.text = user.email;
						if( this.user.meta['_phone'] != null )
							this._ctrlPhone.text = this.user.meta['_phone'];
					});
					
				}
			})
			.catchError( (e) 
			{
				print(e);
			});
		
		SB_Settings.getString('location')
			.then( (d)  
			{
				this.gdata = json.decode(d);
				print('Checkout: ${this.gdata}');
				/*
				SB_Geo.getAddress(this.gdata['lat'], this.gdata['lng'])
					.then( (address) 
					{
						this.setState(()
						{
							this._ctrlAddress.text = address;
						});
					});
				*/
			})
			.catchError( (e) 
			{
				print('ERROR GETTING LOCATION');
			});
		
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(
				title: Text('Información para el pedido'),
			),
			body: SafeArea(
				child: SingleChildScrollView(
					child: Container(
						margin: EdgeInsets.all(10),
						child: Form(
							key: this._formKey,
							child: Column(
								children: [
									Card(
										child: Container(
											padding: EdgeInsets.all(10),
											child: Steps()
										)
									),
									SizedBox(height: 10),
									Totals(),
									SizedBox(height:10),
									UserData(ctrlName: this._ctrlName, ctrlEmail: this._ctrlEmail, ctrlPhone: this._ctrlPhone, ctrlAddress: this._ctrlAddress,),
									SizedBox(height:20),
									WidgetButton(text: 'Enviar pedido', callback: () => this._sendOrder(context), color: Theme.of(context).primaryColor,),
									Row(
										children: [
											/*
											Expanded(
												child: WidgetButton(text: 'Enviar pedido', callback: () => this._sendOrder(context))
											),
											SizedBox(width:10),
											Expanded(
												child: WidgetButton(text: 'Pedido Express', type: 'secondary', icon: FaIcon(FontAwesomeIcons.whatsapp), callback: () 
												{
													var settings = SB_Globals.getVar('settings');
													if( settings['whatsapp_order_number'] != null && this._formKey.currentState.validate() )
													{
														this._buildOrder();
														FlutterOpenWhatsapp.sendSingleMessage(
															settings['whatsapp_order_number'], 
															this._cart.buildExpresOrder(this.user,this.order)
														);
													}
												})
											),
											*/
										]
									)
								]
							)
						)
					)
				)
			)
		);
	}
}
