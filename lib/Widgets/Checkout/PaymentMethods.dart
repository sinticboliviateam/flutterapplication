import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PaymentMethods extends StatefulWidget
{
	final	void Function(String)	methodSelected;
	
	PaymentMethods({this.methodSelected});
	
	@override
	_PaymentMethodsState	createState() => _PaymentMethodsState();
}
class _PaymentMethodsState extends State<PaymentMethods>
{
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Column(
			crossAxisAlignment: CrossAxisAlignment.stretch,
			children: [
				Text('Seleccionar tu método de pago para tu pedido', textAlign: TextAlign.center, style: TextStyle(fontSize: 18, fontWeight:FontWeight.bold)),
				SizedBox(height:15),
				Text('Selecciona tu método preferido de pago para tu pedido, esta información nos ayuda a proporcionarte un servicio más rápido y adecuado.'),
				SizedBox(height: 8),
				Expanded(
					child: ListView(
						children: [
							ListTile(
								leading: Icon(Icons.credit_card, color: Color(0xff0065ab),),
								title: Text('Tarjeta de Crédito/Débito'),
								onTap: ()
								{
									if( this.widget.methodSelected != null )
										this.widget.methodSelected('cc');
									/*
									this._paymentMethod = 'cc';
									this.setState(()
									{
										this._ctrlPaymentMethod.text = 'Tarjeta de Crédito/Débito';
									});
									Navigator.pop(context);
									*/
								},
							),
							ListTile(
								leading: FaIcon(FontAwesomeIcons.fileInvoiceDollar, color: Color(0xff0065ab),), //Icon(Icons.settings_system_daydream),
								title: Text('Tranferencia Bancaria'),
								onTap: ()
								{
									if( this.widget.methodSelected != null )
										this.widget.methodSelected('wire');
								}
							),
							ListTile(
								leading: FaIcon(FontAwesomeIcons.university, color: Color(0xff0065ab),),//Icon(Icons.money_off),
								title: Text('Deposito en Cuenta'),
								onTap: ()
								{
									if( this.widget.methodSelected != null )
										this.widget.methodSelected('bank_account');
								}
							),
							ListTile(
								leading: FaIcon(FontAwesomeIcons.moneyBill, color: Color(0xff0065ab),),//Icon(Icons.attach_money),
								title: Text('Efectivo al momento de la entrega'),
								onTap: ()
								{
									if( this.widget.methodSelected != null )
										this.widget.methodSelected('cash');
								}
							),
						]
					)
				)										
			]
		);
	}
}