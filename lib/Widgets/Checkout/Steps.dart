import 'package:flutter/material.dart';

class Steps extends StatelessWidget
{
	final bool userInfo;
	final bool paymentData;
	final int	step;
	
	Steps({this.userInfo = true, this.paymentData = true, this.step = 1});
	
	@override
	Widget build(BuildContext context)
	{
		return Container(
			child: Row(
				children: [
					Expanded(
						child: Center(child: Icon(Icons.location_off_rounded, color: this.step == 1 ? Colors.redAccent : Colors.black54,)),
					),
					SizedBox(width: 10),
					Text('...', style: TextStyle(fontSize: 22)),
					if( this.paymentData )
						Expanded(
							child: Center(child: Icon(Icons.credit_card)),
						),
					if( this.paymentData )
						SizedBox(width: 10),
					Text('...', style: TextStyle(fontSize: 22)),
					SizedBox(width: 10),
					Expanded(
						child: Center(child: Icon(Icons.check_circle)),
					),
					
				]			
			)
		);
	}
}