import 'package:flutter/material.dart';

class UserData extends StatefulWidget
{
	final TextEditingController	ctrlName;
	final TextEditingController	ctrlEmail;
	final TextEditingController	ctrlPhone;
	final TextEditingController	ctrlAddress;
	
	UserData({this.ctrlName, this.ctrlEmail, this.ctrlPhone, this.ctrlAddress});
	
	@override
	_UserData createState() => _UserData();
}

class _UserData extends State<UserData>
{
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Card(
			child: Container(
				padding: EdgeInsets.all(10),
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						SizedBox(height:10),
						TextFormField(
							decoration: InputDecoration(
								hintText: 'Nombre',
								icon: Icon(Icons.supervisor_account,),
								border: OutlineInputBorder(
									borderSide: BorderSide(
										width: 1,
										style: BorderStyle.solid
									)
								),
							),
							controller: this.widget.ctrlName,
							validator: (v)
							{
								if( v.isEmpty )
									return 'Debe ingresar su nombre';
							}
						),
						SizedBox(height: 8),
						TextFormField(
							decoration: InputDecoration(
								hintText: 'Email',
								icon: Icon(Icons.email,),
								border: OutlineInputBorder(
									borderSide: BorderSide(
										width: 1,
										style: BorderStyle.solid
									)
								),
							),
							keyboardType: TextInputType.emailAddress,
							controller: this.widget.ctrlEmail,
							validator: (v)
							{
								if( v.isEmpty )
									return 'Debe ingresar su email';
							}
						),
						SizedBox(height: 8),
						TextFormField(
							decoration: InputDecoration(
								hintText: 'Teléfono',
								icon: Icon(Icons.settings_phone,),
								border: OutlineInputBorder(
									borderSide: BorderSide(
										width: 1,
										style: BorderStyle.solid
									)
								),
							),
							keyboardType: TextInputType.phone,
							controller: this.widget.ctrlPhone,
							validator: (v)
							{
								if( v.isEmpty )
									return 'Debe ingresar su número de teléfono';
							}
						),
						SizedBox(height: 8),
						/*
						FormGroup(
							label: 'Zona',
							child: Row(
								//crossAxisAlignment: CrossAxisAlignment.stretch, 
								children:[
									Expanded(
										child: DropdownButton(
											hint: Text('Seleccione su zona'),
											onChanged: (zid)
											{
												var barrios = [];
												this.barrioId = null;
												this._zonas.forEach((zona)
												{
													if( zona['id'].toString() == zid.toString() )
													{
														if( zona['barrios'] != null )
														{
															barrios.addAll(zona['barrios']);
														}
													}
												});
												this.setState(()
												{
													this._barrios = barrios;
													this.zonaId = zid.toString();
												});
												
											},
											value: this.zonaId,
											items: this._zonas.map( (zona)
											{
												return DropdownMenuItem<String>(
													value: zona['id'].toString(),
													child: Text(zona['nombre'])
												);
											}).toList()
										)
									)
								]
							)
						),
						FormGroup(
							label: 'Barrio',
							child: Row(
								//crossAxisAlignment: CrossAxisAlignment.stretch, 
								children:[
									Expanded(
										child: DropdownButton(
											hint: Text('Seleccione su barrio'),
											onChanged: (bid)
											{
												this.setState(()
												{
													this.barrioId = bid.toString();
												});
												
											},
											value: this.barrioId,
											items: this._barrios.map( (barrio)
											{
												return DropdownMenuItem<String>(
													value: barrio['id'].toString(),
													child: Text(barrio['nombre'])
												);
											}).toList()
										)
									)
								]
							)
						),
						*/
						TextFormField(
							//style: Theme.of(context).textTheme.body1,
							decoration: InputDecoration(
								hintText: 'Dirección',
								icon: Icon(Icons.location_on_rounded,),
								border: OutlineInputBorder(
									borderSide: BorderSide(
										width: 1,
										style: BorderStyle.solid
									)
								),
							),
							keyboardType: TextInputType.text,
							controller: this.widget.ctrlAddress,
							validator: (v)
							{
								if( v.isEmpty )
									return 'Debe ingresar la direccion para el envio';
							}
						),
						/*
						FormGroup(
							label: 'Fecha para la entrega',
							child: DateTimeField(
								validator: (val)
								{
									if( val == null )
										return 'Debe seleccionar la fecha y hora para la entrega';
								},
								controller: this._ctrlDateTime,
								format: this.dateFormat,
								onShowPicker: (ctx, currentValue) async
								{
									final date = await showDatePicker(
										context: ctx,
										firstDate: DateTime(1900),
										initialDate: currentValue ?? DateTime.now(),
										lastDate: DateTime(2100),
									);
									if( date != null )
									{
										final time = await showTimePicker(context: ctx, initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()));
										return DateTimeField.combine(date, time);
									}
									else
										return currentValue;
								}
							)
						),
						*/
						/*
						FormGroup(
							label: 'Método de Pago',
							child: TextFormField(
								readOnly: true,
								controller: this._ctrlPaymentMethod,
								validator: (val)
								{
									if( val.isEmpty )
										return 'Debe seleccionar el metodo de pago';
								},
								onTap: ()
								{
									this._showPaymentMethods();
								},
							)
						)
						*/
					]
				)
			)
		);
	}
}