import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_html/flutter_html.dart';
//import 'package:flutter_html/style.dart';
//import 'package:flutter_html/html_parser.dart';
//import 'package:share/share.dart';
//import '../WidgetNumberPicker.dart';
//import '../WidgetBottomMenu.dart';
import '../../Classes/MB_Cart.dart';
import '../../Classes/MB_Product.dart';
import 'CountDown.dart';
import '../Checkout/Cart/WidgetCart.dart';
import '../AddToCart.dart';

class WidgetSingleProduct extends StatefulWidget
{
	final MB_Product	product;
	
	WidgetSingleProduct({this.product});
	@override
	_WidgetSingleProductState	createState() => _WidgetSingleProductState();
}
class _WidgetSingleProductState extends State<WidgetSingleProduct>
{
	MB_Cart		_cart = MB_Cart();
	GlobalKey<ScaffoldState>	_scaffoldKey = GlobalKey<ScaffoldState>();
	bool						_expired = false;
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		//int.parse(this.widget.product.getMeta('_to_date_time', '0').toString());
		return Scaffold(
			key: this._scaffoldKey,
			body: Container(
				color: Colors.white,
				child: CustomScrollView(
					slivers: <Widget>[
						SliverAppBar(
							floating: true,
							//title: Text(this.widget.product.product_name),
							backgroundColor: Colors.transparent,
							iconTheme: IconThemeData(
								color: Colors.white
							),
							expandedHeight: 300,
							flexibleSpace: FlexibleSpaceBar(
								background: FittedBox(
									fit: BoxFit.fill,
									child: Image.network(this.widget.product.getThumbnailUrl(), fit: BoxFit.contain,)
								),
								title: Row(
									mainAxisAlignment: MainAxisAlignment.end,
									children: [
										if( !this._expired )
											Container(
												width: 40,
												height: 40,
												decoration: BoxDecoration(
													color: Colors.red[700],
													borderRadius: BorderRadius.circular(40),
												),
												child: IconButton(
													color: Colors.white,
													icon: Icon(Icons.shopping_cart),
													onPressed: ()
													{
														this._showAddToCart();
													},
												)
											),
										SizedBox(width: 5)
									]
								),
							),
							
						),
						SliverList(
						//SliverFixedExtentList(
							//itemExtent: 300,
							delegate: SliverChildListDelegate([
								Container(
									child: Column(
										crossAxisAlignment: CrossAxisAlignment.stretch,
										children: [
											Container(
												color: Theme.of(context).primaryColor, //Color(0xff4ba600),
												padding: EdgeInsets.all(15),
												child: Row(
													children:[
														Expanded(
															child: Text(this.widget.product.product_name, 
																style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white)
															)
														),
														this._buildPrices(),
														/*
														Row(
															children: [
																IconButton(
																	icon: Icon(Icons.share,),
																	color: Color(0xffFF4C58),
																	onPressed: ()
																	{
																		Share.share(this.widget.product.name + ' ' + this.widget.product.permalink);
																	},
																),
																SizedBox(width: 8),
																IconButton(
																	icon: FaIcon(FontAwesomeIcons.solidHeart),
																	color: Color(0xff8D8D8D),
																	onPressed: ()
																	{
																		var alert = AlertDialog(
																			title: Text("Producto adicionado a favoritos"),
																			content: Text("Su producto fue adicionado a sus favoritos."),
																			actions: <Widget>[
																				FlatButton(
																					child: Text("Cerrar"), 
																					onPressed: ()
																					{
																						Navigator.of(context).pop();
																					}
																				)
																			]
																		);
																		showDialog(context: context, builder: (BuildContext ctx){return alert;});
																	}
																)
															]
														)
														*/
													]
												),
											),
											Container(
												color: Theme.of(context).buttonColor,
												padding: EdgeInsets.all(5),
												child: CountDown(
													finalTimestamp: int.parse(this.widget.product.getMeta('_to_date_time').toString()),
													onExpired: ()
													{
														this.setState(()
														{
															this._expired = true;
														});
													}
												)
											),
											Row(
												children:[
													Expanded(
														child: Column(
															crossAxisAlignment: CrossAxisAlignment.stretch,
															children: [
																
															]
														)
													),
													Expanded(
														child: Container(
															/*
															child: WidgetNumberPicker(
																btnColor: Color(0xffFF4C58),
																onQuantityChanged: (qty)
																{
																	var ci = this._cart.productExists(this.widget.product.id);
																	this.setState(()
																	{
																		if( ci != null )
																		{
																			ci.setQuantity(qty);
																		}
																		else
																		{
																			this._cart.addProduct(this.widget.product, qty);	
																		}
																	});
																}
															),
															*/
														)
													)
												]
											),
											SizedBox(height: 15),
											Card(
												child: Container(
													padding: EdgeInsets.all(5),
													child: Column(
														crossAxisAlignment: CrossAxisAlignment.stretch,
														children: [
															Text('Detalles del producto', style: TextStyle(fontWeight: FontWeight.bold)),
															SizedBox(height: 8),
															Html(
																data: this.widget.product.product_description,
																/*
																style: {
																	"div": Style(
																		color: Color(0xff8D8D8D)
																	)
																}
																*/
															)
														]
													)
												)
											)
										]
									)
								)
							]),
						)
					],
				)
			),
			/*
			floatingActionButton: this._cart.getItems().length > 0 ? FloatingActionButton(
				child: Icon(Icons.shopping_cart),
				backgroundColor: Color(0xffFF4C58),
				foregroundColor: Colors.white,
				onPressed: ()
				{
					//Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetCart()));
				},
			) : null,
			*/
			//bottomNavigationBar: WidgetBottomMenu(currentIndex: 1,),
		);
	}
	Widget _buildPrices()
	{
		if( this.widget.product.product_price > this.widget.product.product_price_2 )
		{
			return Column(
				children: [
					Text(this.widget.product.getPrice(), 
						style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white, decoration: TextDecoration.lineThrough)
					),
					Text(this.widget.product.getPrice2(), 
						style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)
					)
				]
			);
		}
		return Text(this.widget.product.getPrice(), 
			style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.white, )
		);
	}
	void _showAddToCart()
	{
		showModalBottomSheet(isScrollControlled: false, context: context, builder: (ctx) 
		{
			return SafeArea(
				child: StatefulBuilder(
					builder: (_ctx, setter)
					{
						return Container(
							//height: 520,
							color: Color(0xFF737373),
							child: Container(
								//height: 450,
								padding: EdgeInsets.all(10),
								decoration: BoxDecoration(
									color: Theme.of(context).canvasColor,
									borderRadius: BorderRadius.only(
										topLeft: Radius.circular(10),
										topRight: Radius.circular(10),
									),
								),
								child: AddToCart(
									product: this.widget.product,
								)
							)
						);
					}
				)
			);
		});
	}
	void _addToCart()
	{
		
		var cartItem = this._cart.addProduct(this.widget.product, 1, this.widget.product.product_price_2);
		this._scaffoldKey.currentState.showSnackBar(SnackBar(
			content: Row(
				children: [
					Text('El producto fue adicionado al pedido'),
					SizedBox(width: 8),
					InkWell(
						child: Text('Ver pedido', style: TextStyle(color: Colors.green)),
						onTap: ()
						{
							Navigator.push(this.context, MaterialPageRoute(builder: (_) => WidgetCart()));
						},
					)
				],
			),
			backgroundColor: Colors.black87,
		));
	}
}
