import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:core';

class CountDown extends StatefulWidget
{
	final DateTime	finalDate;
	final	int		finalTimestamp;
	final	Function	onExpired;
	
	CountDown({this.finalDate, this.finalTimestamp, this.onExpired});
	
	@override
	_CountDown	createState() => _CountDown();
}

class _CountDown extends State<CountDown>
{
	Timer		timeout;
	int			hour		= 0;
	int			minute 		= 0;
	int			second 		= 0;
	int			totalSeconds;
	bool		expired		= false;
	
	@override
	void initState()
	{
		super.initState();
		int currentTime = (DateTime.now().millisecondsSinceEpoch / 1000).toInt();
		//print('Milliseconds: ${currentTime}\nSeconds: ${this.widget.finalTimestamp}');
		if( currentTime > this.widget.finalTimestamp )
			this.expired = true;
			
		int seconds = (this.widget.finalTimestamp - currentTime).toInt() ;
		this.setTimeFromHours(this.secondsToHours(seconds));
		this.timeout = Timer.periodic(Duration(seconds: 1), this.onTimeSecond);
	}
	@override
	void dispose()
	{
		this.timeout.cancel();
		super.dispose();
	}
	@override
	Widget build(BuildContext context)
	{
		return Column(
			crossAxisAlignment: CrossAxisAlignment.stretch,
			children: [
				if( !this.expired )
					this._getTime(),
				if( this.expired )
					Center(
						child: Text('El producto ha expirado', style: TextStyle(fontSize: 35, color: Colors.white), textAlign: TextAlign.center,)
					),
				Center(
					child: Text('Tiempo restante de la oferta', style: TextStyle(fontSize: 14, color: Colors.white))
				)
			]
		);
	}
	Widget _getTime()
	{
		String timeStr = '';
		timeStr += (this.hour < 10 ? '0${this.hour}' : this.hour.toString()) + ':';
		timeStr += (this.minute < 10 ? '0${this.minute}' : this.minute.toString()) + ':';
		timeStr += (this.second < 10 ? '0${this.second}' : this.second.toString());
		
		return Center(
			child: Text(timeStr, style: TextStyle(fontSize: 40, color: Colors.white))
		);
	}
	void onTimeSecond(timer)
	{
		//print('onTimeSecond');
		this.second--;
		
		this.setState(()
		{
			if( this.second <= 0 )
			{
				this.second = 59;
				this.minute--;
				if( this.minute <= 0 )
				{
					this.minute = 59;
					this.hour--;
				}
			}
			if( this.hour <= 0 )
			{
				this.expired = true;
				if( this.widget.onExpired != null )
					this.widget.onExpired();
			}
		});
		
	}
	double secondsToHours(int seconds)
	{
		double hours = (seconds / 3600);
		
		return hours;
	}
	void setTimeFromHours(double hours)
	{
		this.hour 				= hours.floor();
		double hoursDecimal 	= (hours - this.hour);
		double minutes 			= hoursDecimal * 60;
		double minutesDecimal 	= (minutes - minutes.floor());
		this.minute				= minutes.floor();
		this.second				= (minutesDecimal * 60).floor();
		//this.totalSeconds		= (hours * 3600);
		print('${this.hour}: ${this.minute}: ${this.second}');
	}
}