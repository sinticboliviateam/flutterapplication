import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../Classes/MB_Product.dart';
import 'WidgetSingleProduct.dart';


class WidgetProductListItem extends StatelessWidget
{
	MB_Product	product;
	
	WidgetProductListItem({this.product});
	
	@override
	Widget build(BuildContext context)
	{
		var container = Card(
			//padding: EdgeInsets.all(8),
			//height: 300,
			child: Container(
				
				decoration: BoxDecoration(
					//border: Border(bottom: BorderSide(width: 1, color: Colors.black38)),
					//borderRadius: const BorderRadius.all(const Radius.circular(8))
					color: Colors.white
				),
				padding: EdgeInsets.all(8),
				//margin: EdgeInsets.only(bottom: 10),
				child: Row(
					children:[
						Image.network(
							this.product.getThumbnailUrl(), 
							width: 70, height: 70, 
							fit: BoxFit.fill,
							loadingBuilder: (ctx, widget, chunk)
							{
								if( widget != null )
								{
									return widget;
								}
								return CircularProgressIndicator();
							},
						),
						/*
						FadeInImage(
							placeholder: kTransparentImage,
							image: this.product.getThumbnailUrl()
						),
						*/
						Expanded(
							child: Container(
								margin: EdgeInsets.only(right: 8, left: 8),
								child: Container( 
									child: Column(
										mainAxisAlignment: MainAxisAlignment.start,
										crossAxisAlignment: CrossAxisAlignment.stretch,
										children: [
											Text(
												this.product.product_name, 
												textAlign: TextAlign.left, 
												style: TextStyle(fontSize:17)
											),
											SizedBox(height: 5),
											Text('Disponible: ' + (this.product.getMeta('_to_date', ''))),
											SizedBox(height: 5),
											Text('Stock: ' + (this.product.product_quantity > 0 ? this.product.product_quantity.toString() : 'Agotado')),
										]
									) 
								)
							)
						),
						Container( 
							child: this._buildPrices(),
						)
					]
					
				)
			),
		);
		return InkWell(
			child: container, 
			onTap: () 
			{
				Navigator.push(context, MaterialPageRoute(
					builder: (context) => WidgetSingleProduct(product: this.product)
				));
			}
		);
	}
	Widget _buildPrices()
	{
		if( this.product.product_price > this.product.product_price_2 )
			return Column(
				children: [
					Text(this.product.getPrice(), style: TextStyle(decoration: TextDecoration.lineThrough)),
					Text(this.product.getPrice2())
				]
			);
			
		return Text(
			this.product.getPrice(), 
			textAlign: TextAlign.center,
			style: TextStyle(fontWeight: FontWeight.bold)
		);
	}
}
