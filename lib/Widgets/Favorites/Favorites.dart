import 'package:flutter/material.dart';
import '../../Services/ServiceStores.dart';
import '../../Services/ServiceProducts.dart';
import '../../Services/ServiceUsers.dart';
import '../../Classes/MB_Store.dart';
import '../../Classes/MB_Product.dart';
import '../WidgetStoreDetail.dart';
import '../Products/WidgetSingleProduct.dart';

class Favorites extends StatefulWidget
{
	@override
	_FavoritesState		createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites>
{
	String	_current = 'markets';
	List<MB_Store>		_stores = [];
	List<MB_Product>	_products = [];
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(title: Text('Mis Favoritos', style: TextStyle(color: Colors.white))),
			body: Container(
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						Container(
							child: Row(
								mainAxisAlignment: MainAxisAlignment.center,
								children: [
									TextButton(
										style: TextButton.styleFrom(
											primary: this._current == 'markets' ? Colors.white : Theme.of(context).primaryColor,
											backgroundColor: this._current == 'markets' ? Theme.of(context).primaryColor : Colors.transparent,
										),
										child: Text('Comercios'),
										onPressed: this._listComercios,
										
									),
									SizedBox(width: 10),
									TextButton(
										style: TextButton.styleFrom(
											primary: this._current == 'products' ? Colors.white : Theme.of(context).primaryColor,
											backgroundColor: this._current == 'products' ? Theme.of(context).primaryColor : Colors.transparent,
										),
										child: Text('Productos'),
										onPressed: this._listProducts,
									)
								]
								
							)
						),
						SizedBox(height: 10),
						Expanded(
							child: this._current == 'markets' ? this._buildMarkets() : this._buildProducts()
						)
					]
				)
			)
		);
	}
	void _listComercios()
	{
		this.setState(()
		{
			this._current = 'markets';
		});
	}
	void _listProducts()
	{
		this.setState(()
		{
			this._current = 'products';
		});
	}
	Stream<List<MB_Store>> _getMarkets() async*
	{
		var ids = await ServiceStores().getFavorites();
		this._stores = await ServiceStores().getStoresIn(ids);
		
		yield this._stores;
	}
	Widget _buildMarkets()
	{
		return StreamBuilder(
			stream: this._getMarkets(),
			builder: (ctx, snapshot)
			{
				if( !snapshot.hasData )
				{
					return Center(child: CircularProgressIndicator());
				}
				if( snapshot.data.length <= 0 )
					return Center(child: Text('No se encontraron favoritos'));
					
				return ListView.builder(
					itemCount: this._stores.length,
					itemBuilder: (ctx, index)
					{
						return Card(
							elevation: 3,
							shape: RoundedRectangleBorder(
								borderRadius: BorderRadius.circular(5),
							),
							child: Container(
								padding: EdgeInsets.only(top: 8, bottom: 8),
								child: ListTile(
									leading: this._buildImage(this._stores[index].getThumbnail()),
									title: Text(this._stores[index].store_name),
									onTap: ()
									{
										Navigator.push(this.context, 
											MaterialPageRoute(builder: (_) => WidgetStoreDetail(store: this._stores[index]))
										);
									}
								)
							)
						);
					}
				);
			}
		);
	}
	Widget _buildImage(String url)
	{
		return Container(
			width: 60,
			height: 60,
			decoration: BoxDecoration(
				borderRadius: BorderRadius.circular(60),
				boxShadow: [
					BoxShadow(
						color: Colors.grey.withOpacity(0.7),
						spreadRadius: 3,
						blurRadius: 4,
						offset: Offset(0, 0)
					)
				]
			),
			child: ClipRRect(
				borderRadius: BorderRadius.circular(60),
				child: Container(
					//padding: EdgeInsets.all(3),
					child: Image.network(url, fit: BoxFit.cover)
				)
			)
		);
	}
	Stream<List<MB_Product>> _getProducts() async*
	{
		var ids = await ServiceProducts.getFavorites();
		this._products = await ServiceProducts.getIn(ids);
		
		yield this._products;
	}
	Widget _buildProducts()
	{
		return StreamBuilder(
			stream: this._getProducts(),
			builder: (ctx, snapshot)
			{
				if( !snapshot.hasData )
				{
					return Center(child: CircularProgressIndicator());
				}
				if( snapshot.data.length <= 0 )
					return Center(child: Text('No se encontraron favoritos'));
					
				return ListView.builder(
					itemCount: this._products.length,
					itemBuilder: (ctx, index)
					{
						var product = this._products[index];
						return Card(
							elevation: 3,
							shape: RoundedRectangleBorder(
								borderRadius: BorderRadius.circular(5),
							),
							child: Container(
								padding: EdgeInsets.only(top: 8, bottom: 8),
								child: ListTile(
									leading: this._buildImage(product.getThumbnailUrl()),
									title: Text(product.product_name),
									onTap: ()
									{
										Navigator.push(this.context, 
											MaterialPageRoute(builder: (_) => WidgetSingleProduct(product: product))
										);
									}
								)
							)
						);
					}
				);
			}
		);
	}
}
