import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class OpenLayersMap extends StatelessWidget
{
	String	latitude;
	String	longitude;
	String	provider = '';
	
	OpenLayersMap({this.latitude, this.longitude});
	@override
	Widget build(BuildContext context)
	{
		return Container(
			height: 250,
			child: WebView(
				initialUrl: Uri.dataFromString(this._getHtml(), mimeType: 'text/html').toString(),
				javascriptMode: JavascriptMode.unrestricted,
			)
		);
	}
	String _getHtml()
	{
		String html = '''
		<!doctype html>
		<html>
		<head>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
		</head>
		<body>
			<iframe src="https://www.google.com/maps/embed/v1/place?q=${this.latitude},${this.longitude}&key=AIzaSyCESZMrPOcHQH6WTLfOergalTtWEdTc2jc&zoom=19" style="width:100%;height:250px;" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		</body>
		</html>
		''';
		
		return html;
	}
}