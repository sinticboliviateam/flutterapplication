import 'package:flutter/material.dart';
import '../../Services/ServiceMarket.dart';
import '../../Services/ServiceStores.dart';
import '../../Services/ServiceProducts.dart';
import '../Products/WidgetSingleProduct.dart';
import '../WidgetStoreDetail.dart';
import '../WidgetLoading.dart';

class SearchResults extends StatefulWidget
{
	final String keyword;
	
	SearchResults({this.keyword});
	
	_SearchResultsState	createState() => _SearchResultsState();
}
class _SearchResultsState extends State<SearchResults>
{
	List<Map>	_results = [];
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(title: Text('Resultado de Busqueda')),
			body: Container(
				child: StreamBuilder(
					stream: this._search(),
					builder: (ctx, snapshot)
					{
						if( !snapshot.hasData )
							return Center(child: CircularProgressIndicator());
						
						return ListView.builder(
							itemCount: this._results.length,
							itemBuilder: (_ctx, int index)
							{
								return this._results[index]['type'] == 'store' ? 
									this._buildStore(this._results[index]) : 
									this._buildProduct(this._results[index]);
							}
						);
					}
				)
			)
		);
	}
	Stream<List<Map>> _search() async*
	{
		var items = await ServiceMarket().search(this.widget.keyword);
		print('items: $items');
		this._results.addAll(items);
		print(this._results);
		yield this._results;
	}
	Widget _buildStore(Map store)
	{
		return Card(
			elevation: 2,
			child: Container(
				child: ListTile(
					leading: CircleAvatar(
						radius: 20,
						backgroundImage: NetworkImage(store['thumbnail'])
					),
					title: Text(store['name']),
					subtitle: Text('Comercio'),
					onTap: () async
					{
						var _key = GlobalKey<State>();
						WidgetLoading.show(this.context, _key, 'Obteniendo datos...');
						try
						{
							var ostore = await ServiceStores().getStore(int.parse(store['id']));
							WidgetLoading.hide(_key);
							Navigator.push(this.context, MaterialPageRoute(builder: (_) => WidgetStoreDetail(store: ostore)));
						}
						catch(e)
						{
							WidgetLoading.hide(_key);
							print('ERROR: $e');
						}
					},
				)
			)
		);
	}
	Widget _buildProduct(Map product)
	{
		return Card(
			elevation: 2,
			child: Container(
				child: ListTile(
					leading: CircleAvatar(
						radius: 20,
						backgroundImage: NetworkImage(product['thumbnail'])
						
					),
					title: Text(product['name']),
					subtitle: Text('Producto'),
					onTap: () async
					{
						var _key = GlobalKey<State>();
						WidgetLoading.show(this.context, _key, 'Obteniendo datos...');
						try
						{
							var oproduct = await ServiceProducts.getProduct(int.parse(product['id']));
							WidgetLoading.hide(_key);
							Navigator.push(this.context, MaterialPageRoute(builder: (_) => WidgetSingleProduct(product: oproduct)));
						}
						catch(e)
						{
							WidgetLoading.hide(_key);
							print('ERROR: $e');
						}
						
					},
				)
			)
		);
	}
}
