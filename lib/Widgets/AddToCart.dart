import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'WidgetNumberPicker.dart';
import '../Providers/CartProvider.dart';
import '../Classes/MB_Cart.dart';
import '../Classes/MB_Product.dart';
import 'WidgetButton.dart';
import 'Checkout/Cart/WidgetCart.dart';

class AddToCart extends StatefulWidget
{
	
	final MB_Product		product;
	
	AddToCart({@required this.product});
	@override
	_AddToCartState		createState() => _AddToCartState();
}

class _AddToCartState extends State<AddToCart>
{
	MB_Cart		_cart = MB_Cart();
	int			_quantity = 0;
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		var size = MediaQuery.of(context).size;
		return Container(
			child: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				children: [
					Text('Cantidad para el pedido', textAlign: TextAlign.center, style: TextStyle(fontSize: 18, fontWeight:FontWeight.bold)),
					SizedBox(height:15),
					Text('Selecciona la cantidad del producto para tu pedido.'),
					SizedBox(height: 10),
					Row(
						children: [
							Expanded(flex: 2, child: Text('')),
							SizedBox(width: 10),
							Expanded(
								flex: 8,
								child: WidgetNumberPicker(
									btnColor: Theme.of(context).primaryColor,
									maxNumber: this.widget.product.product_quantity,
									initialValue: this._cart.productExists(this.widget.product.product_id) != null ? 
													this._cart.productExists(this.widget.product.product_id).quantity 
													: 
													0,
									onQuantityChanged: (qty)
									{
										var cartItem = this._cart.addProduct(this.widget.product, qty, 0, false);
										Provider.of<CartProvider>(context, listen: false).quantity = this._cart.getItems().length;
										this.setState(()
										{
											this._quantity = qty;
										});
										/*
										if( cartItem.quantity < 10 )
										{
											setter(()
											{
												this._quantity = qty;
												
											});
											
											Provider.of<CartProvider>(context, listen: false).quantity = this._cart.getItems().length;
										}
										*/										
									}
								)
							),
							SizedBox(width: 10),
							Expanded(flex: 2, child: Text('')),
						] 
					),
					SizedBox(height: 10),
					Container(
						child: Row(
							children:[
								Expanded(
									child: WidgetButton(
										text: 'Confirmar',
										//type: 'secondary',
										color: Color(0xff0065ab),//Color(0xfffec700),
										callback: () 
										{
											Navigator.pop(context);
										}
									)
								),
								SizedBox(width: 10),
								AnimatedContainer(
									width: this._quantity <= 0 ? 0 : ((size.width / 2) - 10).toDouble(),
									duration: Duration(milliseconds: 200),
									child: WidgetButton(
										text: 'Pagar ahora',
										type: 'primary', 
										callback: () 
										{
											Navigator.pop(context);
											Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetCart()));
										}
									)
								)
							]
						)
					)
				]
			)
		);
	}
}
