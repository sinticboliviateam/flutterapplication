import 'package:flutter/material.dart';
import '../WidgetButton.dart';
import '../../Services/ServiceUsers.dart';

class ChangePassword extends StatelessWidget
{
	TextEditingController	_ctrlPass 	= TextEditingController();
	TextEditingController	_ctrlRPass 	= TextEditingController();
	final					_formKey	= GlobalKey<FormState>();
	
	void _doRecover(context) async
	{
		if( !this._formKey.currentState.validate() )
			return;
			
		var data = {'password': this._ctrlPass.text, 'rpassword': this._ctrlRPass.text};
		try
		{
			var res = await ServiceUsers.changePass(data);
			showDialog(context: context,
				builder: (ctx)
				{
					return AlertDialog(
						content: Text('La contraseña se cambio exitosamente'),
						actions: <Widget>[
							FlatButton(
								child: Text('Ok'),
								onPressed: ()
								{
									Navigator.pop(context);
									Navigator.pop(context);
								}
							)
						]
					);
				}
			);
		}
		catch(e)
		{
			showDialog(context: context,
				builder: (ctx)
				{
					return AlertDialog(
						content: Text('Ocurrio un error al cambiar la contraseña, intentalo mas tarde'),
						actions: <Widget>[
							FlatButton(
								child: Text('Cerrar'),
								onPressed: ()
								{
									Navigator.pop(context);
								}
							)
						]
					);
				}
			);
		}
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(
				title: Text('Cambio de contraseña')
			),
			body: Container(
				padding: EdgeInsets.all(10),
				child: Form(
					key: this._formKey,
					child: ListView(
						children: [
							Text('Ingresa tu nueva contraseña en los siguientes campos, asegúrate que ambas son exactamente iguales', style: TextStyle(fontSize: 15)),
							SizedBox(height:10),
							TextFormField(
								decoration: InputDecoration(
									hintText: 'Contraseña',
								),
								obscureText: true,
								controller: this._ctrlPass,
								validator: (v)
								{
									if( v.isEmpty )
										return 'Debes ingresar tu contraseña';
								},
							),
							SizedBox(height: 10),
							TextFormField(
								decoration: InputDecoration(
									hintText: 'Repite la Contraseña',
								),
								obscureText: true,
								controller: this._ctrlRPass,
								validator: (v)
								{
									if( v.isEmpty )
										return 'Debes repetir tu contraseña';
									if( v != this._ctrlPass.text )
										return 'Las contraseñas no coinciden';
								},
							),
							WidgetButton(
								text: 'Confirmar',
								type: 'danger',
								callback: () { this._doRecover(context);},
							)
						]
					)
				)
			)
		);
	}
}
