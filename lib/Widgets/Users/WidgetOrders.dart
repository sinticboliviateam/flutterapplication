import 'package:flutter/material.dart';
import 'WidgetOrdersHistory.dart';

class WidgetOrders extends StatelessWidget
{
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			//backgroundColor: Color(0xffff5c1e),
			appBar: AppBar(title: Text('Mis pedidos')),
			body: SafeArea(
				child: Container(
					/*
					decoration: BoxDecoration(
						color: Color(0xfff8ae7d),
						image: DecorationImage(
							image: AssetImage('images/patitas.png'),
							repeat: ImageRepeat.repeat, 
						)
					),
					*/
					child: Column(
						children: [
							Expanded(child:WidgetOrdersHistory())
						]
					)
				)
			)
		);
	}
}
