import 'package:flutter/material.dart';
import '../WidgetLogin.dart';
import '../../Services/ServiceUsers.dart';
import '../../Classes/Exceptions/ExceptionLogin.dart';

class Register extends StatefulWidget
{
	@override
	_Register	createState() => _Register();
}

class _Register extends State<Register>
{
	String _userType;
	GlobalKey<FormState>		_formKey 		= GlobalKey<FormState>();
	GlobalKey<ScaffoldState>	_scaffoldKey	= GlobalKey<ScaffoldState>();
	TextEditingController		_ctrlFullname	= TextEditingController();
	TextEditingController		_ctrlEmail		= TextEditingController();
	TextEditingController		_ctrlPhone		= TextEditingController();
	TextEditingController		_ctrlPass		= TextEditingController();
	TextEditingController		_ctrlRPass		= TextEditingController();
	bool						_processing		= false;
	
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			key: this._scaffoldKey,
			appBar: AppBar(title: Text('Registro de usuario')),
			body: Container(
				decoration: BoxDecoration(
					color: Colors.black87,
					image: DecorationImage(
						fit: BoxFit.cover,
						image: AssetImage('images/bg01.jpg'),
						repeat: ImageRepeat.repeat,
						colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop)
					)
				),
				child: ListView(
					children: [
						SizedBox(height: 15),
						Container(
							child: Column(
								children: [
									Image.asset('images/logo.png'),
									Center(
										child: Text("Aprovechalo", style: TextStyle(color: Colors.white, fontSize: 40))
									)
								]
							),
						),
						SizedBox(height: 20),
						Text('Registro de cuenta', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 25)),
						SizedBox(height: 8),
						Container(
							padding: EdgeInsets.all(20),
							child: Text('Ingresa tus datos para poder crear tu cuenta, recuerda que estos datós serán utilizados para que nosotros y tus clientes puedan contactarte.', style: TextStyle(color: Colors.white, fontSize: 13)),
						),
						this._getForm(),
					]
				)
			)
		);
	}
	Widget _getForm()
	{
		return Form(
			key: this._formKey,
			child: Container(
				margin: EdgeInsets.all(15),
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						Container(
							height: 50,
							padding: EdgeInsets.only(right: 10, left: 10),
							decoration: BoxDecoration(
								color: Colors.white10,
							),
							child: TextFormField(
								controller: this._ctrlFullname,
								keyboardType: TextInputType.text,
								style: TextStyle(color: Colors.white),
								decoration: InputDecoration(
									filled: true,
									fillColor: Colors.transparent,
									icon: Icon(Icons.dynamic_feed, color: Colors.white),
									hintText: "Nombre completo",	
									hintStyle: TextStyle(color: Colors.white),
									border: OutlineInputBorder(
										borderSide: BorderSide.none
									),
								),
								validator: (v)
								{
									if( v.isEmpty )
										return "Debe ingresar su nombre";
								},
							)
						),
						SizedBox(height: 8),
						Container(
							height: 50,
							padding: EdgeInsets.only(right: 10, left: 10),
							decoration: BoxDecoration(
								color: Colors.white10,
							),
							child: TextFormField(
								controller: this._ctrlEmail,
								keyboardType: TextInputType.emailAddress,
								style: TextStyle(color: Colors.white),
								decoration: InputDecoration(
									filled: true,
									fillColor: Colors.transparent,
									icon: Icon(Icons.alternate_email, color: Colors.white),
									hintText: "Email",	
									hintStyle: TextStyle(color: Colors.white),
									border: OutlineInputBorder(
										borderSide: BorderSide.none
									),
								),
								validator: (v)
								{
									if( v.isEmpty )
										return "Debe ingresar su email";
								},
							)
						),
						SizedBox(height: 8),
						Container(
							height: 50,
							padding: EdgeInsets.only(right: 10, left: 10),
							decoration: BoxDecoration(
								color: Colors.white10,
							),
							child: TextFormField(
								controller: this._ctrlPhone,
								keyboardType: TextInputType.phone,
								style: TextStyle(color: Colors.white),
								decoration: InputDecoration(
									filled: true,
									fillColor: Colors.transparent,
									icon: Icon(Icons.settings_phone, color: Colors.white),
									hintText: "Número de telefono",	
									hintStyle: TextStyle(color: Colors.white),
									border: OutlineInputBorder(
										borderSide: BorderSide.none
									),
								),
								validator: (v)
								{
									if( v.isEmpty )
										return "Debe ingresar su email/usuario";
								},
							),
						),
						SizedBox(height: 8),
						Container(
							height: 50,
							padding: EdgeInsets.only(right: 10, left: 10),
							decoration: BoxDecoration(
								color: Colors.white10,
							),
							child: DropdownButton(
								isExpanded: true,
								hint: Text('Selecciona el tipo de usuario', style: TextStyle(color: Colors.white)),
								value: this._userType,
								dropdownColor: Colors.black,
								onChanged: (val)
								{
									this._userType = null;
									this.setState(()
									{
										this._userType = val;
									});
								},
								items: [
									DropdownMenuItem(
										value: 'user',
										child: Container(
											child: Text('Soy usuario', style: TextStyle(color: Colors.white))
										),
									),
									DropdownMenuItem(
										value: 'trader',
										child: Container(
											child: Text('Tengo un comercio', style: TextStyle(color: Colors.white))
										),
									)
								],
							)	
						),
						SizedBox(height: 8),
						Container(
							height: 50,
							padding: EdgeInsets.only(right: 10, left: 10),
							decoration: BoxDecoration(
								color: Colors.white10,
							),
							child: TextFormField(
								controller: this._ctrlPass,
								keyboardType: TextInputType.text,
								obscureText: true,
								style: TextStyle(color: Colors.white),
								decoration: InputDecoration(
									filled: true,
									fillColor: Colors.transparent,
									icon: Icon(Icons.lock, color: Colors.white),
									hintText: "Contraseña",	
									hintStyle: TextStyle(color: Colors.white),
									border: OutlineInputBorder(
										borderSide: BorderSide.none
									),
								),
								validator: (v)
								{
									if( v.isEmpty )
										return "Debes ingresar tu contraseña";
								},
							),
						),
						SizedBox(height: 8),
						Container(
							height: 50,
							padding: EdgeInsets.only(right: 10, left: 10),
							decoration: BoxDecoration(
								color: Colors.white10,
							),
							child: TextFormField(
								controller: this._ctrlRPass,
								keyboardType: TextInputType.text,
								obscureText: true,
								style: TextStyle(color: Colors.white),
								decoration: InputDecoration(
									filled: true,
									fillColor: Colors.transparent,
									icon: Icon(Icons.lock, color: Colors.white),
									hintText: "Repite tu contraseña",	
									hintStyle: TextStyle(color: Colors.white),
									border: OutlineInputBorder(
										borderSide: BorderSide.none
									),
								),
								validator: (v)
								{
									if( v.isEmpty )
										return "Debes repetir tu contraseña";
								},
							),
						),
						SizedBox(height: 9),
						Container(
							padding: EdgeInsets.all(10),
							color: Theme.of(this.context).primaryColor,
							child: InkWell(
								child: this._processing ? 
									Center(
										child: CircularProgressIndicator(),
									) : 
									Text('Registrar mi cuenta', 
										textAlign: TextAlign.center, 
										style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)
									),
								onTap: ()
								{
									this._doRegister();
								},
							)
						),
						
						SizedBox(height: 20),
						/*
						Center(
							child: InkWell(
								child: Text('Tengo una cuenta', style: TextStyle(color: Theme.of(this.context).primaryColor)),
								onTap: ()
								{
									//Navigator.popUntil(this.context, (_) => false);
									Navigator.pushAndRemoveUntil(this.context, 
										MaterialPageRoute(builder: (_) => WidgetLogin()),
										(_) => false
									);
								}
							)
						)
						*/
					]
				)
			)
		);
	}
	void _doRegister() async
	{
		if( this._processing )
			return;
		this.setState( () 
		{
			this._processing = true;
		});
		
		if( !this._formKey.currentState.validate() )
			return;
		if( this._userType == null || this._userType.isEmpty )
		{
			this._scaffoldKey.currentState.showSnackBar(SnackBar(
				content: Text('Debes seleccionar el tipo de usuario'),
				backgroundColor: Colors.redAccent,
			));
			this.setState( () 
			{
				this._processing = false;
			});
			return;
		}
		if( this._ctrlPass.text != this._ctrlRPass.text )
		{
			this._scaffoldKey.currentState.showSnackBar(SnackBar(
				content: Text('Tus contraseñas no coinciden'),
				backgroundColor: Colors.redAccent,
			));
			this.setState( () 
			{
				this._processing = false;
			});
			return;
		}
		try
		{
			var userData = {
				'fullname': this._ctrlFullname.text.trim(),
				'email': this._ctrlEmail.text.trim(),
				'password': this._ctrlPass.text.trim(),
				'meta': {
					'_phone': this._ctrlPhone.text.trim(),
					'_usertype': this._userType,
				}
			};
			var user = await ServiceUsers.register(userData);
			this.setState( () 
			{
				this._processing = false;
			});
			this._scaffoldKey.currentState.showSnackBar(SnackBar(
				content: Text('Tus contraseñas no coinciden', style: TextStyle(color: Theme.of(this.context).primaryColor)),
				backgroundColor: Colors.black,
			));
			Navigator.pop(this.context);
		}
		on ExceptionRegister catch(e)
		{
			this.setState( () 
			{
				this._processing = false;
			});
			this._scaffoldKey.currentState.showSnackBar(SnackBar(
				content: Text(e.getError()),
				backgroundColor: Colors.redAccent,
			));
		}
		catch(e)
		{
			this.setState( () 
			{
				this._processing = false;
			});
			print(e);
			this._scaffoldKey.currentState.showSnackBar(SnackBar(
				content: Text('Ocurrio un error en el registro, intentalo de nuevo'),
				backgroundColor: Colors.redAccent,
			));
		}
	}
}