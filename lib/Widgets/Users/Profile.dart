import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:io';
import '../../Services/ServiceUsers.dart';
import '../../Classes/MB_User.dart';
import '../../Classes/SB_Settings.dart';

class Profile extends StatefulWidget
{
	@override
	_ProfileState	createState() => _ProfileState();
}
class _ProfileState extends State<Profile>
{
	File	_image;
	TextEditingController	_ctrlName 		= TextEditingController();
	TextEditingController	_ctrlLastname 	= TextEditingController();
	TextEditingController	_ctrlPhone		= TextEditingController();
	TextEditingController	_ctrlEmail		= TextEditingController();
	final					_formKey		= GlobalKey<FormState>();
	SB_User					_user;
	Image					_avatarImage;
	
	Future _selectImage() async
	{
		this._image = await ImagePicker.pickImage(source: ImageSource.gallery);
		this.setState(()
		{
			this._avatarImage = Image.file(this._image, width:120, height:120, fit: BoxFit.fill);
		});
	}
	void _saveData(context) async
	{
		if( !this._formKey.currentState.validate() )
			return;
		var user = new SB_User();
		user.user_id 		= this._user.user_id;
		user.first_name 	= this._ctrlName.text;
		user.last_name 		= this._ctrlLastname.text;
		user.email			= this._ctrlEmail.text.trim();
		user.meta['_phone'] = this._ctrlPhone.text.trim();
		
		try
		{
			await ServiceUsers.update(user);
			print('USER UPDATED');
			ServiceUsers.reload();
			if( this._image != null )
			{
				ServiceUsers.uploadAvatar(this._image)
				.then( (res) 
				{
					print('AVATAR UPDATED');
					print(res['data']);
					//##update user into session
					
				});
			}
		}
		catch(e)
		{
			print(e);
		}
	}
	Widget _getForm(context)
	{
		return Form(
			key: this._formKey,
			child: Column(
				children: [
					TextFormField(
						decoration: InputDecoration(
							hintText: 'Nombre',
						),
						controller: this._ctrlName,
						validator: (value)
						{
							if( value.isEmpty )
								return 'Debe ingresar su nombre';
						}
					),
					SizedBox(height: 10),
					TextFormField(
						decoration: InputDecoration(
							hintText: 'Apellido',
						),
						controller: this._ctrlLastname,
						validator: (value)
						{
							if( value.isEmpty )
								return 'Debe ingresar su apellido';
						}
					),
					SizedBox(height: 10),
					TextFormField(
						decoration: InputDecoration(
							hintText: 'Teléfono'
						),
						keyboardType: TextInputType.phone,
						controller: this._ctrlPhone,
						validator: (value)
						{
							if( value.isEmpty )
								return 'Debe ingresar su número de telefono';
						}
					),
					SizedBox(height: 10),
					TextFormField(
						decoration: InputDecoration(
							hintText: 'Email'
						),
						keyboardType: TextInputType.emailAddress,
						controller: this._ctrlEmail,
						validator: (value)
						{
							//return 'Debe ingresar su direccion de email';
						}	
					)
				]
			)
		);
	}
	@override
	void initState()
	{
		super.initState();
		
		//this._avatarImage != null ? Image.file(this._image, width: 120, height: 120, fit: BoxFit.fill,) : 
		ServiceUsers.getCurrentUser().then( (user) 
		{
			//print('GETTING USER SESSION');
			this._user = user;
			this._ctrlName.text		= user.first_name;
			this._ctrlLastname.text	= user.last_name;
			this._ctrlPhone.text	= user.meta['_phone'] != null ? user.meta['_phone'] : '';
			this._ctrlEmail.text 	= user.email;
			//print(this._user.toMap());
			Image image = null;
			if( !this._user.avatar.isEmpty )
			{
				//print(this._user.avatar);
				image = Image.network(this._user.avatar, width:120, height:120, fit: BoxFit.fill);
			}
			else
			{
				image = Image.asset('images/empty-avatar.png', width:120, height:120, fit: BoxFit.fill);
			}
			this.setState(()
			{
				//if( this._image == null )
					this._avatarImage = image;
			});
				
		});
	}
	@override
	Widget build(BuildContext context)
	{
		
		return Scaffold(
			appBar: AppBar(title: Text('Información personal')),
			floatingActionButton: FloatingActionButton(
				onPressed: ()
				{
					this._saveData(context);
				},
				tooltip: 'Guardar datos',
				child: Icon(Icons.save),
			),
			body: Container(
				padding: EdgeInsets.all(5),
				child: ListView(
					children: [
						SizedBox(height:20),
						Center(
							child: InkWell(
								child: Container(
									padding: EdgeInsets.all(10),
									decoration: BoxDecoration(
										borderRadius: BorderRadius.all(Radius.circular(80.0)),
										color: Colors.white,
										border: Border.all(color: Colors.grey[300]),
									),
									child: ClipRRect(
										borderRadius: BorderRadius.circular(80),
										child: this._avatarImage
									)
								),
								onTap: ()
								{
									this._selectImage();
								}
							) 
						),
						SizedBox(height:20),
						Card(
							child: Container(
								padding: EdgeInsets.all(10),
								child: this._getForm(context),
							)
						),
						
					]
				)
			)
		);
	}
}
