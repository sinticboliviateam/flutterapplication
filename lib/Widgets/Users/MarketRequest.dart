import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../WidgetButton.dart';
import '../../Classes/MB_Store.dart';
import '../../Classes/MB_MembershipRequest.dart';
import '../../Services/ServiceMarket.dart';
import '../../Classes/SB_Settings.dart';
import '../WidgetUserHome.dart';

class MarketRequest extends StatefulWidget
{
	@override
	_MarketRequestState		createState() => _MarketRequestState();
}

class _MarketRequestState extends State<MarketRequest>
{
	final GlobalKey<FormState>	_formKey 			= GlobalKey<FormState>();
	TextEditingController		_ctrlFirstname 		= TextEditingController();
	TextEditingController		_ctrlLastname		= TextEditingController();
	TextEditingController		_ctrlEmail			= TextEditingController();
	TextEditingController		_ctrlPhone			= TextEditingController();
	TextEditingController		_ctrlMarketName		= TextEditingController();
	TextEditingController		_ctrlMarketAddress	= TextEditingController();
	bool						sending = false;
	Map							gdata;
	List<dynamic>				_cities = [];
	String						_selectedCity;
	
	@override
	void initState()
	{
		super.initState();
		this._loadData();
	}
	void _loadData() async
	{
		var location = await SB_Settings.getString('location');
		if( location != null )
			this.gdata = json.decode(location);
		this._cities = await ServiceMarket().getCities();
		this.setState((){});
	}
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(title: Text('Solicitud de Apertura de Comercio')),
			body: Container(
				padding: EdgeInsets.all(10),
				child: ListView(
					children: [
						Text('Ingresa los datos de tu comercio para poder enviar tu solicitud, recuerda que todos tus datos deben ser validados antes de aprobar tu solicitud'),
						SizedBox(height: 10),
						this._requestForm()
					]
				)
			)
		);
	}
	InputDecoration _fieldDecoration([String label = '', String hint = ''])
	{
		return InputDecoration(
			contentPadding: EdgeInsets.only(top: 4, right: 8, bottom: 4, left: 8),
			labelText: label,
			border: OutlineInputBorder(
				borderSide: BorderSide(
					color: Colors.red,
					width: 1,
					style: BorderStyle.solid
				)
			)
		);
	}
	Widget _requestForm()
	{
		
		return Form(
			key: this._formKey,
			child: Column(
				children: [
					TextFormField(
						keyboardType: TextInputType.name,
						controller: this._ctrlFirstname,
						decoration: this._fieldDecoration('Nombre'),
						validator: (v)
						{
							if( v.isEmpty )
								return 'Debe ingresar su nombre';
						}
					),
					SizedBox(height: 10),
					TextFormField(
						controller: this._ctrlLastname,
						decoration: this._fieldDecoration('Apellido'),
						validator: (v)
						{
							if( v.isEmpty )
								return 'Debe ingresar su apellido';
						}
					),
					SizedBox(height: 10),
					TextFormField(
						keyboardType: TextInputType.emailAddress,
						controller: this._ctrlEmail,
						decoration: this._fieldDecoration('Email'),
						validator: (v)
						{
							if( v.isEmpty )
								return 'Debe ingresar su teléfono';
						}
					),
					SizedBox(height: 10),
					TextFormField(
						keyboardType: TextInputType.phone,
						controller: this._ctrlPhone,
						decoration: this._fieldDecoration('Teléfono'),
						validator: (v)
						{
							if( v.isEmpty )
								return 'Debe ingresar su teléfono';
						}
					),
					SizedBox(height: 10),
					TextFormField(
						controller: this._ctrlMarketName,
						decoration: this._fieldDecoration('Nombre del Comercio'),
						validator: (v)
						{
							if( v.isEmpty )
								return 'Debe ingresar el nombre de su comercio';
						}
					),
					SizedBox(height: 10),
					TextFormField(
						controller: this._ctrlMarketAddress,
						decoration: this._fieldDecoration('Dirección del Comercio'),
						validator: (v)
						{
							if( v.isEmpty )
								return 'Debe ingresar la direccion del comercio';
						}
					),
					SizedBox(height: 10),
					DropdownButtonFormField(
						isExpanded: true,
						hint: Text('-- ciudad --'),
						decoration: const InputDecoration(
							border: OutlineInputBorder(),
							contentPadding: EdgeInsets.all(4),
						),
						value: this._selectedCity,
						onChanged: (v)
						{
							this.setState(()
							{
								this._selectedCity = v;
							});
						},
						items: this._cities.map((city)
						{
							return DropdownMenuItem(
								value: city,
								child: Text(city)
							);
						}).toList(),
						validator: (v)
						{
							print('City: $v');
							if( v.isEmpty )
								return 'Debes seleccionar una ciudad';
						}
					),
					SizedBox(height: 15),
					if( !this.sending )
					WidgetButton(
						text: 'Enviar Solicitud',
						callback: ()
						{
							this._sendRequest();
						}
					),
					if( this.sending )
						Center(child: CircularProgressIndicator())
				]
			)
		);
	}
	void _sendRequest() async
	{
		if( this.sending )
			return;
			
		if( !this._formKey.currentState.validate() )
			return;
			
		try
		{
			var store = MB_Store();
			store.store_name = this._ctrlMarketName.text.trim();
			store.store_address = this._ctrlMarketAddress.text.trim();
			store.meta.addAll([
				{
					'meta_key': '_request_geo',
					'meta_value': this.gdata
				},
				{
					'meta_key': '_city',
					'meta_value': this._selectedCity,
				}
			]);
			var request = MB_MembershipRequest(
				firstname: this._ctrlFirstname.text.trim(),
				lastname: this._ctrlLastname.text.trim(),
				email: this._ctrlEmail.text.trim(),
				phone: this._ctrlPhone.text.trim(),
				store: store,
			);
			this.setState(()
			{
				this.sending = true;
			});
			var res = await ServiceMarket().sendMembershipRequest(request);
			this.setState(()
			{
				this.sending = false;
			});
			showDialog(
				context: this.context,
				barrierDismissible: false,
				builder: (ctx)
				{
					return AlertDialog(
						title: Text('Solicitud Recibida', textAlign: TextAlign.center),
						content: SingleChildScrollView(
							child: ListBody(
								children: [
									Container(
										child: Center(
											child: FaIcon(FontAwesomeIcons.checkCircle, size: 45, color: Colors.green),
										)
									),
									SizedBox(height: 20),
									Text('Gracias por su interes en formar parte de nuestra plataform "Aprovechalo"'),
									SizedBox(height: 5),
									Text('Su solicitud fue recibida correctamente y esta en proceso de revisión'),
									SizedBox(height: 5),
									Text('Nos comunicaremos con usted en un plazo de 24 horas para poder confirmar la información'),
									Text('Te invitamos a que puedas explorar nuestra plataforma y encontrar algo que sea de tu interes.')
								]
							)
						),
						actions: [
							TextButton(
								child: Text('Explorar contenidos'),
								onPressed: ()
								{
									Navigator.pushAndRemoveUntil(this.context, MaterialPageRoute(builder: (_ctx) => WidgetUserHome()), (_) => false);
								}
							)
						]
					);
				}
			);
		}
		catch(e)
		{
			this.setState(()
			{
				this.sending = false;
			});
			print(e);
			ScaffoldMessenger.of(this.context).showSnackBar(SnackBar(
				content: Text(e.message)
			));
		}
	}
}
