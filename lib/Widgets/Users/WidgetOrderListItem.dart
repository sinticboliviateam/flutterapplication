import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../Classes/MB_Order.dart';
import 'WidgetOrderDetails.dart';

class WidgetOrderListItem extends StatelessWidget
{
	MB_Order	order;
	
	WidgetOrderListItem({this.order})
	{
		
	}
	void onTapOrder(context)
	{
		Navigator.push(context, MaterialPageRoute( builder: (ctx) => WidgetOrderDetails(order: this.order) ));
	}
	@override
	Widget build(BuildContext context)
	{
		var border_color = Colors.red;
		if( this.order.status == 'COMPLETE' )
		{
			border_color = Colors.green;
		}
		else if( this.order.status == 'PENDING' )
		{
			border_color = Colors.orange;
		}
		else if( this.order.status == 'PROCESSING' )
		{
			border_color = Colors.blue;
		}
		else if( this.order.status == 'DELIVERED' )
		{
			border_color = Colors.yellow;
		}
		return InkWell(
			onTap: () => this.onTapOrder(context),
			child: Card(
				child: Container(
					padding: EdgeInsets.all(10),
					decoration: BoxDecoration(
						border: Border(
							left: BorderSide(
								color: border_color,
								width: 4
							)
						)
					),
					child: Row(
						children:[
							Expanded(
								child: Column(
									children:[
										Row(
											children:[
												Container(
													//alignment: Alignment(0, 0),
													child: Text('Pedido #' + this.order.sequence.toString(), 
														textAlign: TextAlign.left, 
														style: TextStyle(fontWeight: FontWeight.bold)
													)
												)
											]
										),
										SizedBox(height:10),
										Row(
											children: [
												Text('Fecha: ' + DateFormat('d MMMM yyyy').format(this.order.getDate()).toString() 
													//style: TextStyle(fontWeight: FontWeight.bold)
												),
												
											]
										),
										Row(children: [ 
											Text('Estado: ' + this.order.getStatus())
											]
										)
									]
								)
							),
							Column(
								children:[
									Text('Bs. ' + this.order.total.toStringAsFixed(2),
										style: TextStyle(fontWeight:FontWeight.bold)
									),
								]
							)
						]
					)
				)
			)
		);
	}
}