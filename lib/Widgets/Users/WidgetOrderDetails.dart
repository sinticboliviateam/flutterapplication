import 'package:flutter/material.dart';
import '../../Classes/MB_Order.dart';
import 'package:intl/intl.dart';

class WidgetOrderDetails extends StatelessWidget
{
	MB_Order	order;
	
	WidgetOrderDetails({this.order})
	{
	
	}
	List<Widget> getItems()
	{
		var items = <Widget>[];
		this.order.items.forEach((item)
		{
			items.add(
				Card(
					child: Container(
						padding: EdgeInsets.all(10),
						child: Column(
							children: [
								Row( children:[
									Expanded(child: Text(item.qty.toString() + ' x ' + item.name) ),
									Text( (item.qty * item.price).toStringAsFixed(2) ),
								]),
							]
						)
					)
				)
			);
		});
		return items;
	}
	@override
	Widget build(BuildContext context)
	{
		var listViewItems = <Widget>[
			Card(
				child: Container(
					padding: EdgeInsets.all(10),
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.stretch,
						children: [
							Row( children:[Text('Pedido: #', style: TextStyle(fontWeight: FontWeight.bold)), Text(this.order.sequence.toString())]),
							Row( children:[
								Text('Fecha: ',	style: TextStyle(fontWeight: FontWeight.bold)),
								Text(DateFormat('d MMMM yyyy H:mm').format(this.order.getDate()).toString())
							]),
							Row( children:[Text('Estado: ', style: TextStyle(fontWeight: FontWeight.bold)), Text(this.order.getStatus())]),
							Row( children:[Text('Estado del pago: ', style: TextStyle(fontWeight: FontWeight.bold)), Text(this.order.getPaymentStatus())]),
						]
					)
				)
			),
			Container(
				padding: EdgeInsets.all(10),
				child: Center(
					child:Text('Items', 
						textAlign: TextAlign.center, 
						style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
					) 
				)
			),				
		] + this.getItems();
		return Scaffold(
			//backgroundColor: Color(0xffff5c1e),
			appBar: AppBar(title: Text('Detalles del pedido')),
			body: SafeArea(
				child: Container(
					/*
					decoration: BoxDecoration(
						color: Color(0xfff8ae7d),
						image: DecorationImage(
							image: AssetImage('images/patitas.png'),
							repeat: ImageRepeat.repeat, 
						)
					),
					*/
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.stretch,
						children: [
							this._getTotals(),
							Expanded(child: ListView(
								children: listViewItems	
							)),
							
						]
					)
				)
			)
		);
	}
	Widget _getTotals()
	{
		return Card(
			child: Container(
				padding: EdgeInsets.all(20),
			margin: EdgeInsets.only(top:6, right: 3, bottom:6, left: 3),
			/*
			decoration: BoxDecoration(
				color: Color(0xffff5c1e),
				borderRadius: BorderRadius.circular(10),
			),
			*/
			child: Column(
				children:[
					Row(
						children: [
							Text('Envio: ', style: TextStyle(fontSize:20, color: Colors.black, fontWeight: FontWeight.bold),),
							Expanded(child: Container(
							//height: 60,
							child: Text(
								'' + this.order.shipping_cost.toStringAsFixed(2) + ' Bs',
								style: TextStyle(fontSize:20, color: Colors.black, fontWeight: FontWeight.bold),
								textAlign: TextAlign.right
							)
							))
						]
					),
					Row(
						children: [
							Text('Total: ', style: TextStyle(fontSize:20, color: Colors.black, fontWeight: FontWeight.bold),),
							Expanded(
								child: Container(
									//height: 60,
									margin: EdgeInsets.only(top:6, right: 3, bottom:6, left: 3),
									child: Text(
										'' + this.order.total.toStringAsFixed(2) + ' Bs',
										style: TextStyle(fontSize:20, color: Colors.black, fontWeight: FontWeight.bold),
										textAlign: TextAlign.right
									)
								)
							)
						]
					)
				]
			)
		));

	}
}
