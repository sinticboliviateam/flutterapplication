import 'package:flutter/material.dart';
import 'Profile.dart';
import 'ChangePassword.dart';
import '../../Classes/SB_Globals.dart';

class Account extends StatefulWidget
{
	@override
	_AccountState	createState() => _AccountState();
}
class _AccountState extends State<Account> with RouteAware
{
	@override
	Widget build(BuildContext context)
	{
		return Scaffold(
			appBar: AppBar(title: Text('Mi cuenta')),
			body: ListView(
				children:[
					Card(
						elevation: 2,
						child: ListTile(
							leading: Icon(Icons.assignment_ind),
							title: Text('Información personal'),
							trailing: Icon(Icons.chevron_right),
							onTap:(){
								Navigator.push(context, MaterialPageRoute(builder: (ctx) => Profile()));
							}
						)
					),
					Card(
						elevation: 2,
						child: ListTile(
							leading: Icon(Icons.https),
							title: Text('Cambiar contraseña'),
							trailing: Icon(Icons.chevron_right),
							onTap:()
							{
								Navigator.push(context, MaterialPageRoute(builder: (ctx) => ChangePassword()));
							}
						)
					)
				]
			)
		);
	}
	@override
	void didChangeDependencies()
	{
		super.didChangeDependencies();
		print('WidgetAccount -> didChangeDependencies');
		if( SB_Globals.getVar('floating_avatar') != null )
		{
			(SB_Globals.getVar('floating_avatar') as OverlayEntry).remove();
			SB_Globals.setVar('floating_avatar', null);
		}
	}
}
