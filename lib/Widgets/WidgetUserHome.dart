import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Services/ServiceStores.dart';
import '../Services/ServiceProducts.dart';
import '../Providers/SessionProvider.dart';
import 'WidgetStoreDetail.dart';
import 'WidgetSideDrawer.dart';
import '../Helpers/GeoLocationHelper.dart';
import '../Classes/Coordinate.dart';
import '../Classes/SB_Settings.dart';
import 'SearchSettings.dart';
import 'Stores/CategoryStore.dart';
import 'BottomMenu.dart';
import 'Search/SearchResults.dart';
import 'Ads/ads.dart';

class WidgetUserHome extends StatefulWidget
{
	@override
	_WidgetUserHomeState	createState() => _WidgetUserHomeState();
}
class _WidgetUserHomeState extends State<StatefulWidget>
{
	Coordinate cUser = null;
	GlobalKey<ScaffoldState>	_scaffoldKey = GlobalKey<ScaffoldState>();
	@override
	void initState()
	{
		super.initState();
		this._loadData();
		//new Coordinate(latitude: -16.468790, longitude: -68.151544);
	}
	void _loadData() async
	{
		SB_Settings.getObject('location').then( (l) 
		{
			this.setState( () 
			{
				this.cUser = new Coordinate.fromMap(l);
			});
				
		});
		Provider.of<SessionProvider>(context, listen: false).favoriteStores = await ServiceStores().getFavorites();
		Provider.of<SessionProvider>(context, listen: false).favoriteProducts = await ServiceProducts.getFavorites();
	}
	@override
	Widget build(BuildContext context)
	{
		return FutureBuilder(
			future: ServiceProducts.getMarketCategories(),
			builder: (ctx, snapshot) 
			{
				if( snapshot.hasData )
				{
					return DefaultTabController(
						length: snapshot.data.length,
						child: Scaffold(
							key: this._scaffoldKey,
							drawer: WidgetSideDrawer(),
							endDrawer: SearchSettings(),
							bottomNavigationBar: BottomMenu(itemIndex: 0),
							appBar: AppBar(
								title: Text('Bienvenido', style: TextStyle(color: Colors.white)),
								/*Column(
									children: [
										,
										TextFormField(
											
										)
									]
								),*/
								iconTheme: IconThemeData(color: Colors.white),
								bottom: PreferredSize(
									preferredSize: Size.fromHeight(100.0),
									child: Column(
										children: [
											this._buildSearch(),
											TabBar(
												isScrollable: true,
												tabs: [
													for(int i = 0; i < snapshot.data.length; i++)
														Tab(
															child: Text(
																snapshot.data[i]['name'], 
																style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)
															)
														),
												]
											)
										]
									),
								),
								actions: [Container()]
							),
							body: TabBarView(
								children: [
									for(int i = 0; i < snapshot.data.length; i++)
										this._buildCategoryContent(snapshot.data[i]),
								]
							)
						),
					);
				}
				return Container(
					color: Colors.white,
					child: Center(
						child: CircularProgressIndicator()
					)
				);
			},
		);
	}
	Widget _buildSearch()
	{
		return Container(
			height: 50,
			padding: EdgeInsets.only(left: 6, right: 6),
			child: Row(
				children: [
					Expanded(
						child: TextFormField(
							decoration: InputDecoration(
								hintText: 'Buscar...',
								filled: true,
								fillColor: Colors.white,
								contentPadding: EdgeInsets.all(5),
								border: OutlineInputBorder(
									borderSide: BorderSide(
										style: BorderStyle.solid
									),
									borderRadius: BorderRadius.circular(5)
								)
							),
							style: TextStyle(
							
								backgroundColor: Colors.white,
							),
							onFieldSubmitted: (keyword)
							{
								print('Keyword: $keyword');
								if( keyword.isEmpty )
									return;
								Navigator.push(context, MaterialPageRoute(builder: (_) => SearchResults(keyword: keyword)));
							}
						)
					),
					SizedBox(width: 10),
					IconButton(
						icon: Icon(Icons.tune, color: Colors.white),
						iconSize: 25,
						onPressed: ()
						{
							this._scaffoldKey.currentState.openEndDrawer();
						}
					)
				]
			)
		);
	}
	Widget _buildCategoryContent(Map category)
	{
		return Container(
			padding: EdgeInsets.all(8),
			child: FutureBuilder(
				future: ServiceProducts.getMarketCategoryStores(int.parse(category['id'])),
				builder: (context, snapshot) 
				{
					if( snapshot.hasData )
					{
						var storesList = <Widget>[];
						snapshot.data.forEach((store) 
						{
							var cMarket = new Coordinate(
								latitude: double.tryParse(store.getMeta('_lat')),
								longitude: double.tryParse(store.getMeta('_lng')),
								description: 'Location: ${store.store_name}'
							);
							double distance = this.cUser != null ? GeoLocationHelper.calculateDistance(this.cUser, cMarket) : 0;
							storesList.add(CategoryStore(distance: distance, store: store));
							if( storesList.length == 3 )
							{
								storesList.add( WidgetAds() );
							}
						});
						return ListView(
							children: storesList
						);
					}
					return Center(
						child: CircularProgressIndicator()
					);
				},
			)
		);
	}
}
