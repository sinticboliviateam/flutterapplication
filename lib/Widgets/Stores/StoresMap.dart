import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_html/flutter_html.dart';
import 'dart:io';
import 'dart:convert';
import '../../Classes/Coordinate.dart';
import '../../Classes/MB_Store.dart';
import '../WidgetStoreDetail.dart';
import '../../Services/ServiceStores.dart';

class StoresMap extends StatefulWidget
{
	final Coordinate		center;
	final void Function(StoresMapController)	onCreated;
	
	StoresMap({this.center, this.onCreated});
	
	@override
	_StoresMapState	createState() => _StoresMapState();
}
class StoresMapController
{
	void Function(Coordinate) setPosition;
	
}
class _StoresMapState extends State<StoresMap>
{
	WebViewController	_controller;
	StoresMapController	_ctrlMap 	= StoresMapController();
	String				mapUrl 		= 'https://aprovechalo.hexadatos.net/mobile/map';
	int index 						= 1;
	
	@override
	void initState()
	{
		super.initState();
		if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
		//##expose widget methods
		this._ctrlMap.setPosition = this.setPosition;
		
		if( this.widget.onCreated != null )
			this.widget.onCreated(this._ctrlMap);
	}
	@override
	Widget build(BuildContext context)
	{
		return Container(
			child: IndexedStack(
				index: this.index,
				children: [
					WebView(
						initialUrl: this.mapUrl + '?lat=${this.widget.center.latitude}&lng=${this.widget.center.longitude}',
						javascriptMode: JavascriptMode.unrestricted,
						javascriptChannels: <JavascriptChannel>[
							JavascriptChannel(
								name: 'mob_on_store_clicked',
								onMessageReceived: this._onStoreClicked,
							)
						
						].toSet(),
						onWebViewCreated: (WebViewController ctrl)
						{
							this._controller = ctrl;
						},
						onPageFinished: (finish) 
						{
							this.setState( ()
							{
								this.index = 0;
							});
						}
					),
					Center(child: CircularProgressIndicator())
				]
			)
		);
	}
	void setPosition(Coordinate coords)
	{
		print('Updating user position');
		String js = 'update_user_position({latitude: ${coords.latitude}, longitude: ${coords.longitude}})';
		print(js);
		this._controller.evaluateJavascript(js);
	}
	bool	lock = false;
	void _onStoreClicked(JavascriptMessage msg) async
	{
		if( this.lock )
			return;
		this.lock = true;
		var data = json.decode(msg.message);
		print(data);
		var store = await ServiceStores().getStore(int.tryParse(data['store_id'].toString()));
		this._showStoreInfo(store);
		
	}
	Future<void> _showStoreInfo(MB_Store store) async
	{
		
		return showDialog<void>(
			context: this.context, 
			barrierDismissible: false,
			builder: (BuildContext ctx)
			{
				return WillPopScope(
					onWillPop: () async
					{
						print('onWillPop');
						this.lock = false;
						return true;
					},
					child: AlertDialog(
						title: Text('Informacion del comercio'),
						/*
						content: Container(
							child:Column(
								crossAxisAlignment: CrossAxisAlignment.stretch,
								children: [
									 Row(
										children: [
											Container(
												child: Image.asset('images/store-45x45.png', fit: BoxFit.cover)
											),
											SizedBox(width: 10),
											Column(
												children: [
													Text(store.store_name, style: TextStyle(fontWeight: FontWeight.bold))
												]
											)
										]
									),
									Container(
										child: Html(
											data: store.description,
										)
									),
									SizedBox(height: 15),
									TextButton(
										style: TextButton.styleFrom(
											primary: Colors.white,
											backgroundColor: Theme.of(ctx).primaryColor,
										),
										child: Text('Ver comercio'),
										onPressed: ()
										{
											this.lock = false;
										}
									),
									SizedBox(height: 8),
									TextButton(
										style: TextButton.styleFrom(
											primary: Colors.white,
											backgroundColor: Colors.red,
										),
										child: Text('Cerrar'),
										onPressed: ()
										{
											this.lock = false;
											Navigator.of(ctx).pop();
										}
									),
								]
							)
						),
						*/
						content: SingleChildScrollView(
							child: ListBody(
							children: <Widget>[
								Row(
									children: [
										Container(
											child: Image.asset('images/store-45x45.png', fit: BoxFit.cover)
										),
										SizedBox(width: 10),
										Column(
											children: [
												Text(store.store_name, style: TextStyle(fontWeight: FontWeight.bold))
											]
										)
									]
								),
								Container(
									child: Html(
										data: store.about.isEmpty || store.about == null ? 
											'No existe descripción del comercio' : store.about,
									)
								),
								SizedBox(height: 15),
								TextButton(
									style: TextButton.styleFrom(
										primary: Colors.white,
										backgroundColor: Theme.of(ctx).primaryColor,
									),
									child: Text('Ver comercio'),
									onPressed: ()
									{
										this.lock = false;
										Navigator.pop(ctx);
										Navigator.push(ctx, MaterialPageRoute(builder: (_) => WidgetStoreDetail(store: store)));
									}
								),
								SizedBox(height: 8),
								TextButton(
									style: TextButton.styleFrom(
										primary: Colors.white,
										backgroundColor: Colors.red,
									),
									child: Text('Cerrar'),
									onPressed: ()
									{
										this.lock = false;
										Navigator.of(ctx).pop();
									}
								),
							],
						  ),
						),
						/*
						actions: <Widget>[
							TextButton(
								child: Text('Cerrar'),
								onPressed: () 
								{
									Navigator.of(ctx).pop();
								},
							),
						],
						*/
					)
				);
			}
		);
	}
}
