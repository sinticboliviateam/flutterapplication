import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../Providers/SessionProvider.dart';
import '../../Classes/MB_Store.dart';
import '../WidgetStoreDetail.dart';
import '../../Services/ServiceStores.dart';
import '../../Providers/SessionProvider.dart';
import '../WidgetLogin.dart';

class CategoryStore extends StatefulWidget
{
	final 	double			distance;
	final	MB_Store		store;
	
	CategoryStore({this.distance = 0, this.store});
	@override
	_CategoryStoreState	createState()	=> _CategoryStoreState();
}

class _CategoryStoreState extends State<CategoryStore>
{
	bool _inFavorites = false;
	SessionProvider		_provider;
	
	@override
	void initState()
	{
		super.initState();
		
		/*
		if( this._provider.favoriteStores.contains(this.widget.store.store_id) )
		{
			this.setState(() 
			{
				this._inFavorites = true;
			});
		}
		*/
	}
	@override
	Widget build(BuildContext context)
	{
		this._provider = Provider.of<SessionProvider>(context);
		return InkWell(
			child: Card(
				elevation: 3,
				child: Container(
					height: 180,
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.stretch,
						children: [
							Expanded(
								child: Stack(
									fit: StackFit.expand,
									children: [
										Container(
											width: double.infinity,
											height: double.infinity,
											child: this.widget.store.getImage() != null ? 
												Image.network(this.widget.store.getThumbnail(), fit: BoxFit.cover) 
												:
												Container()
										),
										Positioned(
											top: 10,
											left: 10,
											child: Container(
												decoration: BoxDecoration(
													color: Colors.black.withOpacity(0.4),
													borderRadius: BorderRadius.circular(40),
												),
												child: InkWell(
													child: IconButton(
														icon: Icon(
															this._provider.hasStoreFavorite(this.widget.store.store_id) ? Icons.favorite : Icons.favorite_border, 
															color: this._provider.hasStoreFavorite(this.widget.store.store_id) ? Colors.red : Colors.white.withOpacity(0.7)
														)
													),
													onTap: this._onAddFavorite
												)
											)
										)
									]
								),
							),
							SizedBox(height: 10),
							Container(
								padding: EdgeInsets.all(2),
								child: Column(
									children: [
										Container(
											
											child: Text(this.widget.store.store_name, textAlign: TextAlign.center, 
												style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
											),
										),
										SizedBox(height: 8),
										Row(
											mainAxisAlignment: MainAxisAlignment.spaceBetween,
											children: [
												Container(
													margin: EdgeInsets.only(left: 5),
													child: Text('Distancia: ' + ( this.widget.distance > 0 ? '${this.widget.distance}km' : 'desconocida' ) ) 
												),
												Container(
													padding: EdgeInsets.only(top: 4, right: 5, bottom: 4, left: 5),
													margin: EdgeInsets.only(right: 5),
													decoration: BoxDecoration(
														color: Theme.of(context).primaryColor, //Color(0xff44ab9d)
														borderRadius: BorderRadius.circular(8),
													),
													child: Text('Abierto', 
														textAlign: TextAlign.right, 
														style: TextStyle(color: Colors.white)
													)
												),
											]
										),
										SizedBox(height: 3),
									]
								)
							)
						]
					)
				),
			),
			onTap: ()
			{
				Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetStoreDetail(store: this.widget.store)));
			},
		);
	}
	void _onAddFavorite() async
	{
		if( !this._provider.authenticated )
		{
			ScaffoldMessenger.of(this.context).showSnackBar(
				SnackBar(
					backgroundColor: Colors.red,
					content: Text('Debes iniciar sesion para adicionar favoritos')
				)
			);
			//Navigator.push(this.context, MaterialPageRoute(builder: (_) => WidgetLogin(type: 'user')));
			return;
		}
		String message = '';
		if( this._inFavorites )
		{
			message = 'El comercio se quito de favoritos';
			ServiceStores().removeFavorite(this.widget.store.store_id);
			this.setState( () { this._inFavorites = false; });
		}
		else
		{
			message = 'El comercio se adiciono a sus favoritos';
			ServiceStores().addFavorite(this.widget.store.store_id);
			this.setState( () { this._inFavorites = true; });
		}
		ScaffoldMessenger.of(this.context).showSnackBar(SnackBar(content: Text(message)));
	}
}
