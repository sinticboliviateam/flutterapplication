import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Providers/SearchProvider.dart';

class SearchSettings extends StatefulWidget
{
	@override
	_SearchSettingsState	createState() => _SearchSettingsState();
}

class _SearchSettingsState extends State<SearchSettings>
{
	@override
	void initState()
	{
		super.initState();
	}
	@override
	Widget build(BuildContext context)
	{
		var provider = Provider.of<SearchProvider>(context);
		return Drawer(
			child: Container(
				child: ListView(
					children: [
						ListTile(
							title: Text('Preferencias de busqueda', style: TextStyle(color: Colors.white)),
							tileColor: Theme.of(this.context).primaryColor,
						),
						ListTile(
							title: Text('Buscar comercios cercanos'),
							leading: Icon(Icons.location_searching),
							trailing: Switch(
								value: provider.nearby,
								onChanged: (v)
								{
									provider.nearby = v;
								}
							)
						),
						if( provider.nearby )
							ListTile(
								title: DropdownButton(
									isExpanded: true,
									hint: Text('Distance'),
									value: provider.distance,
									onChanged: (v)
									{
										provider.distance = v;
									},
									items: [
										DropdownMenuItem(
											value: 3,
											child: Text('A menos de 3 kilometros')
										),
										DropdownMenuItem(
											value: 5,
											child: Text('A menos de 5 kilometros')
										),
										DropdownMenuItem(
											value: 7,
											child: Text('A menos de 7 kilometros')
										),
										DropdownMenuItem(
											value: 10,
											child: Text('A menos de 10 kilometros')
										),
										DropdownMenuItem(
											value: -1,
											child: Text('Cualquier distancia')
										),
									]
								),
								leading: Icon(Icons.settings_ethernet_outlined)
							),
						ListTile(
							title: Text('Mostrar comercios abiertos'),
							leading: Icon(Icons.home_work_outlined),
							trailing: Switch(
								value: provider.closed,
								onChanged: (v)
								{
									provider.closed = v;
								}
							)
						),
						SizedBox(height: 50),
						Center(
							child: FlatButton(
								color: Theme.of(context).primaryColor,
								child: Text('Aceptar', style: TextStyle(color: Colors.white)),
								onPressed: ()
								{
									Navigator.pop(context);
								}
							)
						)
						
					]
				)
			)
		);
	}
}
