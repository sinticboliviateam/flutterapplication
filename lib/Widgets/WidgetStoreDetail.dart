import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import '../Providers/SessionProvider.dart';
import '../Classes/MB_Store.dart';
import '../Classes/MB_Product.dart';
import 'Products/WidgetProductListItem.dart';
import '../Services/ServiceProducts.dart';
import '../Services/ServiceStores.dart';
import 'OpenLayersMap.dart';
import '../Helpers/GeoLocationHelper.dart';
import '../Classes/SB_Settings.dart';
import '../Classes/MB_User.dart';
import '../Classes/Coordinate.dart';

class WidgetStoreDetail extends StatefulWidget
{
	final MB_Store	store;
	
	WidgetStoreDetail({this.store});
	
	_WidgetStoreDetailState	createState() => _WidgetStoreDetailState();
}
class _WidgetStoreDetailState extends State<WidgetStoreDetail> with SingleTickerProviderStateMixin
{
	TabController		_tabController;
	List<MB_Product>	_products;
	Coordinate		cUser;
	double			distance = 0;
	bool			_inFavorites = false;
	
	@override
	void initState()
	{
		this._tabController = TabController(length: 2, vsync: this);
		super.initState();
		SB_Settings.getObject('location').then( (l) 
		{
			var cMarket = new Coordinate(
				latitude: double.parse(this.widget.store.getMeta('_lat')),
				longitude: double.parse(this.widget.store.getMeta('_lng'))
			);

			this.setState( () 
			{
				if( Provider.of<SessionProvider>(this.context, listen: false).favoriteStores.contains(this.widget.store.store_id) )
				{
					this._inFavorites = true;
					
				}
				this.cUser = new Coordinate.fromMap(l);
				this.distance = GeoLocationHelper.calculateDistance(this.cUser, cMarket);
			});
				
		});
		
	}
	@override
	Widget build(BuildContext context)
	{
		var hours = this.widget.store.getBusinessHours();
		return Scaffold(
			//appBar: AppBar(title: Text('')),
			body: NestedScrollView(
				headerSliverBuilder: (_, b)
				{
					return <Widget>[
						SliverAppBar(
							iconTheme: IconThemeData(
								color: Colors.white,
							),
							floating: true,
							title: Text(''),
							backgroundColor: Colors.transparent,
							expandedHeight: 220,
							flexibleSpace: FlexibleSpaceBar(
								background: FittedBox(
									fit: BoxFit.fill,
									child: this.widget.store.getImage() != null ? Image.network(this.widget.store.getImage()['mediumThumbnailUrl'], fit: BoxFit.cover) : Container()
								)
							)
						),
						SliverToBoxAdapter(
							child: Column(
								crossAxisAlignment: CrossAxisAlignment.stretch,
								children: [
									Container(
										color: Theme.of(context).primaryColor, //Color(0xff4ba600),
										padding: EdgeInsets.all(5),
										child: Row(
											children: [
												Expanded(
													child: Column(
														crossAxisAlignment: CrossAxisAlignment.stretch,
														children: [
															Text(this.widget.store.store_name, style: TextStyle(color: Colors.white, fontSize: 20)),
															SizedBox(height: 3),
															Text(
																'Horarios: ' + (hours != null ? hours['open'] : '00:00') + ' - ' +
																(hours != null ? hours['close'] : '00:00'),
																style: TextStyle(color: Colors.white)
															),
															SizedBox(height: 3),
															Text('Distancia: ${this.distance}km', style: TextStyle(color: Colors.white)),
														]
													)
												),
												SizedBox(width: 10),
												IconButton(
													icon: Icon(Icons.favorite,
														color: this._inFavorites ? Colors.red : null
													),
													onPressed: this._inFavorites ? this._removeFromFavorites : this._addFavorite,
												)
											]
										)
									),
									Container(
										color: Theme.of(context).primaryColor,
										child: TabBar(
											isScrollable: true,
											controller: this._tabController,
											tabs: [
												Tab(child: Text('Productos'),),
												Tab(child: Text('Acerca del comercio'),),
												
												//Tab(child: Text('Contactos'),),
											]
										)
									),
								]
							)
						)
					];
				},
				body: TabBarView(
					controller: this._tabController,
					children: <Widget>[
						this._buildProducts(),
						Container(
							child: ListView(
								children: [
									Card(
										child: Container(
											padding: EdgeInsets.all(8),
											child: Column(
												crossAxisAlignment: CrossAxisAlignment.stretch,
												children:[
													Text('Acerca de comercio', textAlign: TextAlign.left, style: TextStyle(fontWeight: FontWeight.bold),),
													SizedBox(height: 10),
													Html(
														data: this.widget.store.getMeta('_about_trader') ?? ''
													),
												] 
											)
										)
									),
									SizedBox(height: 15), 
									Card(
										child: Container(
											padding: EdgeInsets.all(8),
											child: Column(
												crossAxisAlignment: CrossAxisAlignment.stretch,
												children: [
													Text('Contactos', style: TextStyle(fontWeight: FontWeight.bold),),
													SizedBox(height: 10),
													Text('Teléfono: ' + (this.widget.store.getMeta('_phone') ?? '')),
												]
											)
										),
									),
									SizedBox(height: 15),
									Card(
										child: Container(
											padding: EdgeInsets.all(8),
											child: Column(
												crossAxisAlignment: CrossAxisAlignment.stretch,
												children: [
													Text('Dirección', style: TextStyle(fontWeight: FontWeight.bold),),
													SizedBox(height: 10),
													Html(
														data: this.widget.store.store_address.replaceAll('\n', '<br>')
													),
												]
											)
										),
									),
									SizedBox(height: 15),
									Card(
										child: Container(
											padding: EdgeInsets.all(8),
											child: Column(
												crossAxisAlignment: CrossAxisAlignment.stretch,
												children: [
													Text('Ubicacion', style: TextStyle(fontWeight: FontWeight.bold),),
													SizedBox(height: 10),
													OpenLayersMap(latitude: this.widget.store.getMeta('_lat'), longitude: this.widget.store.getMeta('_lng'),)
												]
											)
										),
									)
								]
							)
						),
						
					],
				),
			),
			/*
			bottomNavigationBar: Container(
				child: CustomScrollView(
					primary: true,
					shrinkWrap: !false,
					slivers: [
						SliverAppBar(
							floating: true,
							title: Text(''),
							backgroundColor: Colors.transparent,
							expandedHeight: 220,
							flexibleSpace: FlexibleSpaceBar(
								background: FittedBox(
									fit: BoxFit.fill,
									child: this.widget.store.getImage() != null ? Image.network(this.widget.store.getImage()['mediumThumbnailUrl'], fit: BoxFit.cover) : Container()
								)
							)
						),
						SliverList(
							delegate: SliverChildListDelegate([
								
							])
						),
					]
				)
			),
			*/
		);
	}
	Widget _buildProducts()
	{
		return FutureBuilder(
			future: ServiceProducts.getProducts(this.widget.store.store_id),
			builder: (ctx, snapshot)
			{
				if( snapshot.hasData )
				{
					if( snapshot.data.length <= 0 )
						return Container(
							padding: EdgeInsets.all(15),
							child: Text('No encontraron productos para este comercio', textAlign: TextAlign.center)
						);
						
					this._products = snapshot.data;
					return Column(
						children: this._products.map( (product) => WidgetProductListItem(product: product,) ).toList()
					);
				}
				return Center(child: CircularProgressIndicator());
			},
		);
	}
	void _addFavorite() async
	{
		Provider.of<SessionProvider>(this.context, listen: false).favoriteStores = await ServiceStores().addFavorite(this.widget.store.store_id);
		this.setState( () 
		{
			this._inFavorites = true;
		});
	}
	void _removeFromFavorites() async
	{
		Provider.of<SessionProvider>(this.context, listen: false).favoriteStores = await ServiceStores().removeFavorite(this.widget.store.store_id);
		this.setState( () 
		{
			this._inFavorites = false;
		});
	}
}
