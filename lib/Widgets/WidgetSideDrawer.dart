import 'package:flutter/material.dart';
import 'WidgetLogin.dart';
import '../Services/ServiceUsers.dart';
import '../Classes/SB_Settings.dart';
import '../main.dart';
//import 'WidgetMap.dart';
import 'Users/Account.dart';
//import 'Pages/WidgetPage.dart';
import '../Classes/SB_Globals.dart';
import 'Users/WidgetOrders.dart';
import '../Pages/Page.dart';

class WidgetSideDrawer extends  StatefulWidget
{
	@override
	WidgetSideDrawerState	createState() => WidgetSideDrawerState();
}
class WidgetSideDrawerState extends State<WidgetSideDrawer>
{
	List<Widget>		_items = [];
	Function			t;
	
	void logout(context)
	{
		ServiceUsers.closeSession();
		Navigator.pop(context);
		Navigator.of(context).popUntil((route) => route.isFirst);
		//Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetLogin()));
		Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => WidgetLogin()), (_) => false);
	}
	Widget getIcon(IconData icon)
	{
		return Icon(icon, color: Colors.black87);
	}
	Future<void> _buildItems(context) async
	{
		var user 			= await ServiceUsers.getCurrentUser();
		bool authenticated 	= await ServiceUsers.isLoggedIn();
		String token 		= await SB_Settings.getString('token');
		dynamic	address		= await SB_Settings.getObject('current_address');
		var settings		= SB_Globals.getVar('settings', {});
		//print('Current token: ${token}');
		var _items 			= <Widget>[];
		_items.add( new UserAccountsDrawerHeader(
			accountName: user != null ? Text(user.fullname) : Text('Invitado'),
			accountEmail: Column(
				crossAxisAlignment: CrossAxisAlignment.stretch,
				children: [
					user != null ? Text(user.email) : Text(''),
					if( address != null )
						SizedBox(height: 8), Text(address != null ? address['address'] : '', style: TextStyle(fontWeight: FontWeight.bold))
				]
			),
			currentAccountPicture: Row(
				children: [
					Expanded(
						child: Center(
							child: CircleAvatar(
								backgroundImage: user != null ? NetworkImage(user.avatar) : AssetImage('images/no-image.jpg'),
								radius: 50
							)
						)
					),
				]
			),
			
			/*
			decoration: BoxDecoration(
				image: DecorationImage(
					image: NetworkImage(user.avatar),
					fit: BoxFit.cover
				)
			)
			*/
		));
		double fontSize = 15;
		var align = TextAlign.left;
		var style = TextStyle(color: Colors.black87, fontSize: fontSize);
		_items.add(
			new ListTile(
				leading: this.getIcon(Icons.home),
				title: Text('Inicio', textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					//Navigator.push(context, MaterialPageRoute(builder: (ctx) => MyHomePage()));
				}
			),
		);
		
		_items.add(new ListTile(
			leading: this.getIcon(Icons.help), 
			title: Text('Ayuda', textAlign: align, style: style),
			onTap: ()
			{
				Navigator.pop(context);
				
				settings['help_page'] = 3;
				
				if( settings['help_page'] != null )
					Navigator.push(context, MaterialPageRoute(builder: (context) => PageContent(id: settings['help_page'],)));
				
			}
		));
		if( authenticated )
		{
			/*
			_items.add(new ListTile(
				leading: this.getIcon(Icons.my_location),
				title: Text('Mi Dirección', textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetMap()));
				}
			));
			*/
			_items.add(new ListTile(
				leading: this.getIcon(Icons.account_circle),
				title: Text('Mis Pedidos', textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetOrders()));

				}
			));
			_items.add(new ListTile(
				leading: this.getIcon(Icons.account_circle),
				title: Text('Mi Perfil', textAlign: align, style: style),
				onTap: ()
				{
					Navigator.pop(context);
					Navigator.push(context, MaterialPageRoute(builder: (context) => Account()));
				}
			));
			_items.add(new ListTile(
				leading: this.getIcon(Icons.power_settings_new), 
				title: Text('Salir', textAlign: align, style: style), 
				onTap: () => this.logout(context)));
		}
		else
		{
			_items.add(new ListTile(
				leading: this.getIcon(Icons.vpn_key),
				title: Text('Iniciar sesión', textAlign: align, style: style), 
				onTap: () 
				{
					Navigator.pop(context);
					Navigator.push(context, MaterialPageRoute(builder: (context) => WidgetLogin()));
				}
			));
		}
		this.setState( ()
		{
			this._items = _items;
		});
	}
	@override
	void initState()
	{
		super.initState();
	}	
	@override
	Widget build(BuildContext context)
	{
		this._buildItems(context);
		return Theme(
			data: Theme.of(context).copyWith(
				//canvasColor: Color(0xffff5c1e).withOpacity(0.6),//Colors.blue[900].withOpacity(0.6),
			),
			child: new Drawer(
				child: ListView(
					children: this._items
				)
			)
		);
	}
}
