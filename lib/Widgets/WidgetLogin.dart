import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import '../Providers/SessionProvider.dart';
import 'WidgetUserHome.dart';
import '../Services/ServiceUsers.dart';
import '../Classes/Exceptions/ExceptionLogin.dart';
import '../Classes/SB_Settings.dart';
import 'WidgetLoading.dart';
import 'Users/Register.dart';
import 'Users/MarketRequest.dart';

class WidgetLogin extends StatefulWidget
{
	final String type;
	
	WidgetLogin({this.type});
	@override
	WidgetLoginState createState() => WidgetLoginState();
}

class WidgetLoginState extends State<WidgetLogin>
{
	final GlobalKey<ScaffoldState>	_scaffoldKey	= GlobalKey<ScaffoldState>(); 
	final GlobalKey					_formKey		= GlobalKey<FormState>();
	TextEditingController			_ctrlUsername 	= TextEditingController();
	TextEditingController			_ctrlPassword 	= TextEditingController();
	bool							_processing		= false;
	Color							_color;
	@override
	void initState()
	{
		super.initState();
	}
	Widget build(BuildContext context)
	{
		this._color = this.widget.type == 'user' ? Theme.of(context).buttonColor : Theme.of(context).primaryColor;
		var leftMargin = MediaQuery.of(context).size.width * 0.2;
		
		return Scaffold(
			key: this._scaffoldKey,
			appBar: AppBar(title: Text("Ingreso de usuario", style: TextStyle(color: Colors.white)), backgroundColor: this._color,),
			body: Container(
				decoration: BoxDecoration(
					color: Colors.black87,
					image: DecorationImage(
						fit: BoxFit.cover,
						image: AssetImage('images/bg01.jpg'),
						repeat: ImageRepeat.repeat,
						colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop)
					)
				),
				child: ListView(
					children: <Widget>[
						Container(
							child: Column(
								children: [
									Image.asset('images/logo.png'),
									Center(
										child: Text("Aprovechalo", style: TextStyle(color: Colors.white, fontSize: 40))
									)
								]
							),
						),
						SizedBox(height: 20),
						Container(
							child: Opacity(
								opacity: 1,
								child: Container(
									decoration: BoxDecoration(
										color: Colors.transparent, //Color(0x3effffff),
										borderRadius: BorderRadius.only(
											topLeft: Radius.circular(10),
											topRight: Radius.circular(10),
											bottomRight: Radius.circular(10),
											bottomLeft: Radius.circular(10),
										)
									),
									margin: EdgeInsets.all(20),
									child: this._loginForm(),
								),
							)
						)
					],
				)
			)
		);
	}
	Widget _loginForm()
	{
		return Form(
			key: this._formKey,
			child: Container(
				padding: EdgeInsets.all(10),
				child: Column(
					children: [
						Text('Credenciales de acceso', 
							style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)
						),
						SizedBox(height: 15),
						Container(
							height: 50,
							padding: EdgeInsets.only(right: 10, left: 10),
							decoration: BoxDecoration(
								color: Colors.white10,
								/*
								boxShadow: [
									BoxShadow(
										color: Colors.grey,
										offset: Offset(3, 3),
										blurRadius: 5
									)
								]
								*/
							),
							child: TextFormField(
								controller: this._ctrlUsername,
								keyboardType: TextInputType.emailAddress,
								style: TextStyle(color: Colors.white),
								decoration: InputDecoration(
									filled: true,
									fillColor: Colors.transparent,
									icon: Icon(Icons.supervisor_account, color: Colors.white),
									hintText: "Usuario/Email",	
									hintStyle: TextStyle(color: Colors.white),
									border: OutlineInputBorder(
										borderSide: BorderSide.none/*(
											style: BorderStyle.solid
										)*/
									),
								),
								validator: (v)
								{
									if( v.isEmpty )
										return "Debe ingresar su email/usuario";
								},
							),
							
						),
						SizedBox(height: 10),
						Container(
							height: 50,
							color: Colors.white10,
							padding: EdgeInsets.only(right: 10, left: 10),
							/*
							decoration: BoxDecoration(
								color: Colors.white,
								boxShadow: [
									BoxShadow(
										color: Colors.grey,
										offset: Offset(3, 3),
										blurRadius: 5
									)
								]
							),
							*/
							child: TextFormField(
								controller: this._ctrlPassword,
								obscureText: true,
								decoration: InputDecoration(
									filled: true,
									fillColor: Colors.transparent,
									icon: Icon(Icons.lock, color: Colors.white,),
									hintText: "Contraseña",
									hintStyle: TextStyle(color: Colors.white),
									border: OutlineInputBorder(
										borderSide: BorderSide.none/*(
											width: 0,
											style: BorderStyle.solid
										)*/
									),
								),
								validator: (v)
								{
									if( v.isEmpty )
										return "Debe ingresar su contraseña";
								},
							)
						),
						SizedBox(height: 15),
						Container(
							//padding: EdgeInsets.all(10),
							child: Container(
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.stretch,
									children: [
										Container(
											color: this._color,
											child: InkWell(
												child: Container(
													padding: EdgeInsets.all(10),
													child: this._processing ?
														Center(
															child: SizedBox(
																width: 20,
																height: 20,
																child: CircularProgressIndicator(
																	backgroundColor: Colors.white
																)
															)
														) :
														Text("Ingresar", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,) 
												),
												onTap: ()
												{
													this._doLogin();
												},
											)
										),
										SizedBox(height: 20),
										Container(
											child: Row(
												mainAxisAlignment: MainAxisAlignment.center,
												children: [
													Container(
														width: 40,
														height: 40,
														color: Colors.blue[900],
														child: Center(
															child: InkWell(
																child: FaIcon(FontAwesomeIcons.facebookF, color: Colors.white,),
															)
														)
													),
													SizedBox(width: 20),
													Container(
														width: 40,
														height: 40,
														color: Colors.red[700],
														child: Center(
															child: InkWell(
																child: FaIcon(FontAwesomeIcons.googlePlusG, color: Colors.white,),
															)
														)
													)
												]
											)
										),
										SizedBox(height: 20),
										Container(
											//color: color,
											child: Row(
												children: [
													Expanded(
														child: InkWell(
															child: Text('Olvidaste tu contraseña?', style: TextStyle(color: Colors.white))
														)
													),
													SizedBox(width: 8),
													Expanded(
														child: InkWell(
															child: Container(
																padding: EdgeInsets.all(10),
																child: Text(
																	this.widget.type == 'company' ? 'Solicitar apertura de comercio' : "Crear cuenta",
																	style: TextStyle(color: Theme.of(context).primaryColor), 
																	textAlign: TextAlign.center,
																) 
															),
															onTap: ()
															{
																/*
																Navigator.push(context, 
																	MaterialPageRoute(
																		builder: (_) => Register(),
																	)
																);
																*/
																Navigator.push(context,
																	PageRouteBuilder(
																		pageBuilder: (ctx, animation, sa) => this.widget.type == 'company' ? MarketRequest() : Register(),
																		transitionsBuilder: (ctx, animation, sa, w) 
																		{
																			/*
																			return RotationTransition(
																				turns: animation,
																				child: w
																			);
																			*/
																			return ScaleTransition(
																				scale: animation,
																				child: w
																			);
																			
																			/*
																			var begin = Offset(0.0, 1.0);
																			  var end = Offset.zero;
																			  var tween = Tween(begin: begin, end: end);
																			  var offsetAnimation = animation.drive(tween);
																			
																			  return SlideTransition(
																				position: offsetAnimation,
																				child: w,
																			  );
																			*/
																		},
																		//transitionDuration: Duration(milliseconds: 1000)	
																	)
																);
															},
														)
													)
												]
											)
										),
									]
								),
							)
						)
					]
				)
			)
		);
	}
	void _doLogin() async
	{
		if( this._processing )
			return;
		this.setState(() 
		{
			this._processing = true;
		} );
		
		var _key = GlobalKey<State>();
		WidgetLoading.show(this.context, _key, 'Validando credenciales');
		try
		{
			var user = await ServiceUsers.login(this._ctrlUsername.text.trim(), this._ctrlPassword.text);
			SB_Settings.saveObject('user', user.toMap());
			this.setState(() 
			{
				this._processing = false;
			} );
			Provider.of<SessionProvider>(this.context, listen: false).authenticated = true;
			WidgetLoading.hide(_key);
			Navigator.pushAndRemoveUntil(this.context, MaterialPageRoute(builder: (ctx) =>  WidgetUserHome()), (_) => true);
		}
		on ExceptionLogin catch(e)
		{
			this.setState(() 
			{
				this._processing = false;
			} );
			WidgetLoading.hide(_key);
			this._scaffoldKey.currentState.showSnackBar(SnackBar(
				content: Text(e.getError()),
				backgroundColor: Colors.redAccent,
			));
		}
		catch(e)
		{
			this.setState(() 
			{
				this._processing = false;
			});
			WidgetLoading.hide(_key);
			print(e);
			this._scaffoldKey.currentState.showSnackBar(SnackBar(
				content: Text('Ocurrio un error en el proceso de login, intentelo de nuevo'),
				backgroundColor: Colors.redAccent,
			));
		}
	}
}
