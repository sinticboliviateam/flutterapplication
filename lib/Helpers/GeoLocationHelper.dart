import 'dart:math';
import 'package:geolocator/geolocator.dart';
import 'dart:convert';
import 'dart:async';
import '../Classes/Coordinate.dart';

class GeoLocationHelper
{
	static double calculateDistance(Coordinate from, Coordinate to, [String measure = 'km'])
	{
		if( !from.isValid() )
		{
			print('FROM: Invalid coordinates for: ${from.description}');
			return 0;
		}
		if( !to.isValid() )
		{
			print('TO: Invalid coordinates for: ${to.description}');
			return 0;
		}
		double distance = sqrt(
			pow(69.1 * (from.latitude - to.latitude), 2) +
			pow(69.1 * (from.longitude - to.longitude) * cos(to.latitude / 57.3), 2)
		);
		
		if( measure != 'km' )
			return num.parse(distance.toStringAsFixed(2));
			
		return num.parse( (distance * 1.60934).toStringAsFixed(2) );
	}
	static Future<Coordinate> getLocation() async
	{
		try
		{
			print('Getting location');
			//Geolocator().checkGeolocationPermissionStatus();
			var position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
			print(position);
			var cor = new Coordinate(latitude: position.latitude, longitude: position.longitude, description: 'User coordinates');
			return cor;
			
		}
		catch(e)
		{
			print(e);
			print("Get position error");
		}
	}
	static StreamSubscription<Position> _positionStream;
	
	static void startFollowLocation(double min, [void Function(Coordinate) changed]) async
	{
		Coordinate last;
		stopFollowLocation();
		print('Starting follow location');
		_positionStream = getPositionStream().listen((Position position) async
		{
			if( last == null )
			{
				print('First location');
				last = Coordinate(latitude: position.latitude, longitude: position.longitude);
			}
			try
			{
				var current 	= Coordinate(latitude: position.latitude, longitude: position.longitude);
				print('getPositionStream: ${current.toMap()}');
				double distance = calculateDistance(last, current);
				print('Distance: $distance >= $min');
				if( distance >= min )
				{
					print('Location/Position changed');
					print(position);
					last = current;
					if( changed != null )
						changed(current);
				}
			}
			catch(e)
			{
				print(e);
			}
		});
	}
	static void stopFollowLocation()
	{
		if( _positionStream == null )
			return;
		print('Stopping follow location');
		_positionStream.cancel();
		_positionStream = null;
	}
}
