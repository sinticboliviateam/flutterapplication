
class SB_User
{
	int 	user_id;
	String	first_name;
	String	last_name;
	String	email;
	String	username;
	String	address_1;
	String	address_2;
	String	avatar;
	
	Map<String, dynamic> meta = {};
	
	String get fullname
	{
		return this.first_name + ' ' + this.last_name;
	}
	SB_User()
	{
	}
	SB_User.fromJson(Map<dynamic, dynamic> json)
	{
		this.loadData(json);
	}
	void loadData(Map<dynamic, dynamic> json)
	{
		this.user_id 	= json['user_id'];
		this.first_name = json['first_name'];
		this.last_name	= json['last_name'];
		this.email		= json['email'];
		this.username	= json['username'];
		this.address_1	= json['address_1'];
		this.address_2	= json['address_2'];
		this.avatar		= json['avatar'] != null ? json['avatar'] : '';
		if( json['meta'] != null )
		{
			if( json['meta'] is Map )
			{
				(json['meta'] as Map).forEach((meta_key, meta_value) 
				{
					this.meta[meta_key] = meta_value;
				});
			}
			else if( json['meta'] is List )
			{
				(json['meta'] as List).forEach((meta) 
				{
					this.meta[meta['meta_key']] = meta['meta_value'];
				});
			}
		}
	}
	Map<String, dynamic> toMap() =>
	{
		'user_id': this.user_id,
		'first_name': this.first_name,
		'last_name': this.last_name,
		'email': this.email,
		'username': this.username,
		'address_1': this.address_1,
		'address_2': this.address_2,
		'avatar': this.avatar,
		'meta': this.meta
	};
}

class MB_User extends SB_User 
{
	MB_User.fromJson(Map<dynamic, dynamic> data)
	{
		this.loadData(data);
	}
}