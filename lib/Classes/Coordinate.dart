class Coordinate
{
	double	latitude;
	double	longitude;
	String	description;
	
	Coordinate({this.latitude, this.longitude, this.description});
	Coordinate.fromMap(Map<dynamic, dynamic> data)
	{
		this.loadData(data);
	}
	void loadData(Map data)
	{
		if( data == null )
			return;
		this.latitude 		= data['latitude'] != null ? double.parse(data['latitude'].toString()) : 0;
		this.longitude 		= data['longitude'] != null ? double.parse(data['longitude'].toString()) : 0;
		this.description 	= data['description'] != null ? data['description'].toString() : '';
	}
	Map<String, dynamic> toMap()
	{
		return {
			'latitude': this.latitude,
			'longitude': this.longitude,
			'description': this.description,
		};
	}
	bool isValid()
	{
		if( this.latitude.toString().isEmpty || this.latitude == null || this.longitude.toString().isEmpty || this.longitude == null )
			return false;
		return true;
	}
}
