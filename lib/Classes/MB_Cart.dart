import 'MB_CartItem.dart';
import 'MB_Product.dart';
import 'MB_User.dart';
import 'MB_Order.dart';
import 'SB_Globals.dart';
import '../Services/ServiceUsers.dart';

class MB_Cart
{
	List<MB_CartItem> _items;
	double	_total = 0;
	static final MB_Cart _instance = MB_Cart._privateConstructor();
	
	MB_Cart._privateConstructor()
	{
		this._items = new List();
	}
	factory MB_Cart()
	{
		return _instance;
	}
	MB_CartItem addProduct(MB_Product product, [int qty = 1, double price = 0, bool increase = true])
	{
		var cartItem = this.productExists(product.product_id);
		if( cartItem == null )
		{
			cartItem = new MB_CartItem(product: product, quantity: qty);
			this._items.add( cartItem );
			if( price > 0 )
				cartItem.price = price;
		}
		else
		{
			if( /*cartItem.quantity < 10*/ increase )
				cartItem.increaseQuantity(qty);
			else
				cartItem.quantity = qty;
		}
		return cartItem;
	}
	void removeProduct(int productId)
	{
		var toRemove = null;
		this._items.forEach( (item) 
		{
			if( item.product.product_id == productId )
			{
				toRemove = item;
			}
		});
		if( toRemove != null )
		{
			this._items.remove(toRemove);
		}
	}
	MB_CartItem productExists(productId)
	{
		var cartItem = null;
		
		this._items.forEach( (item) 
		{
			if( item.product.product_id == productId )
			{
				cartItem = item;
			}
		});
		
		return cartItem;
	}
	void clear()
	{
		this._items = new List();
	}
	double calculateTotals()
	{
		this._total = 0;
		this._items.forEach( (MB_CartItem item) 
		{
			this._total += item.getTotal();
		});
		
		return this._total;
	}
	double getTotals()
	{
		return this.calculateTotals();
	}
	List getItems()
	{
		return this._items;
	}
	double getDeliveryCost()
	{
		var settings 			= SB_Globals.getVar('settings');
		if( settings == null )
			return 0;
			
		double delivery_amount 	= settings['delivery_amount'] != null ? double.parse(settings['delivery_amount'].toString()) : 0;
		double total 			= this.getTotals();
		double min_delivery_amount = settings['delivery_free_amount'] != null ? double.parse(settings['delivery_free_amount'].toString()) : 0;
		
		if( total >= min_delivery_amount )
		{
			delivery_amount = 0;
		}
		return delivery_amount;
	}
	String buildExpresOrder(MB_User user, MB_Order order)
	{
		//String mapLink = 'https://www.google.com/maps/embed/v1/place?q=-16.540571399999997,-68.0708216&key=AIzaSyBJgGD8MYJOl8U1ya9u2QVF-BmaoQBIpRA';
		String order_str = '';
		int i = 1;
		this._items.forEach((item)
		{
			order_str += '${i}. ' + item.quantity.toString() + ' x ' + item.product.getName() + ' ' + item.total.toStringAsFixed(2) + " Bs\n";
		});
		order_str += '\n\n';
		order_str += 'Total pedido: ' + this.getTotals().toStringAsFixed(2) + ' Bs\n';
		order_str += 'Costo Delivery: ' + this.getDeliveryCost().toStringAsFixed(2) + ' Bs\n';
		order_str += 'Total a pagar: ' + (this.getTotals() + this.getDeliveryCost()).toStringAsFixed(2) + ' Bs\n';
		order_str += '\n\nCliente: ${user.fullname}\n';
		order_str += 'Telefono: ${order.customer_phone}\n';
		order_str += 'Dirección: ${order.meta['_shipping_address']}\n';
		order_str += 'Método de Pago: ${order.meta['_payment_method']}\n';
		order_str += 'Fecha de entrega: ${order.delivery_date}\n';
		order_str += 'Ubicacion:\n https://maps.google.com/maps?q=${order.meta['_lat']},${order.meta['_lng']}&hl=es-BO&gl=bo&shorturl=1';
		
		print(order_str);
		return order_str;
	}
}
