import 'package:flutter/material.dart';

class MB_Attribute
{
	int		id;
	String	name;
	String	description;
	String	data;
	String	type;
	List	values = [];
	
	MB_Attribute();
	
	MB_Attribute.fromJson(Map<String, dynamic> _json)
	{
		this.loadData(_json);
	}
	void loadData(Map<String, dynamic> data)
	{
		this.id 			= data['id'];
		this.name 			= data['name'];
		this.description	= data['description'];
		this.data			= data['data'];
		this.type			= data['type'];
		if( data['values'] != null )
		{
			this.values = data['values'];
		}
	}
	Map<String, dynamic> toMap()
	{
		var map = {
			'id': 			this.id,
			'name': 		this.name,
			'description': 	this.description,
			'data':			this.data,
			'type':			this.type
		};
		return map;
	}
	Map<String, dynamic> getValueById(int id)
	{
		var _value = null;
		this.values.forEach((val)
		{
			if( val['id'] == id )
			{
				_value = val;
			}
		});
		
		return _value;
	}
}