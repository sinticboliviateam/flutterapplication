import 'dart:convert';
import 'SB_Object.dart';

class MB_Store //extends SB_Object
{
	int					store_id;
	String				code;
	String				store_name;
	String				store_address;
	String				store_description;
	List<dynamic>		images;
	List<dynamic>		meta = [];
	
	
	String get description
	{
		return this.store_description != null ? this.store_description : '';
	}
	String	get	about
	{
		return this.getMeta('_about_trader');
	}
	MB_Store()
	{
		
	}
	MB_Store.fromMap(Map<String, dynamic> json)
	{
		//var store = MB_Store();
		this.loadData(json);
		
		//return store;
	}
	void loadData(Map<String, dynamic> data)
	{
		try
		{
			//super.loadData(data);
			this.store_id 			= int.tryParse(data['store_id'].toString());
			this.code				= data['code'];
			this.store_name 		= data['store_name'];
			this.store_description	= data['store_description'];
			this.store_address		= data['store_address'];
			if( data['images'] != null && data['images'] is List )
			{
				this.images = data['images'];
			}
			
			if( data['meta'] != null && data['meta'] is List )
			{
				this.meta = data['meta'];
			}
			/*
			if( data['about'] != null )
				this.about = data['about'];
			*/
		}
		catch(e)
		{
			print('MB_Store ERROR: $e');
		}
		
	}
	Map<String, dynamic> toMap()
	{
		var data = {
			'store_id': this.store_id,
			'store_name': this.store_name,
			'store_address': this.store_address,
			'store_description': this.store_description,
			'code': this.code,
			'meta': []
		};
		if( this.meta != null )
			data['meta'] = this.meta;
		return data;
	}
	Map getImage()
	{
		if( this.images == null || this.images.length <= 0 )
			return null;
		
		return this.images[0];
	}
	String getThumbnail()
	{
		var image = this.getImage();
		if( image == null )
			return null;
			
		return image['mediumThumbnailUrl'];
		/*
		static Map image;
		if( image != null )
			return image['mediumThumbnailUrl'];
		image = this.widget.store.getImage();
		return image['mediumThumbnailUrl'];
		*/
	}
	String getMeta(key)
	{
		if( this.meta == null )
			return '';
		if( this.meta.length <= 0 )
			return '';
			
		Map _meta = {};
		
		this.meta.forEach( (m) 
		{
			if( m.containsKey('meta_key') && m['meta_key'] == key )
				_meta = m;
		});
		
		return _meta.containsKey('meta_value') && _meta['meta_value'] != null ? _meta['meta_value'].toString() : '';
	}
	Map getBusinessHours()
	{
		var meta = this.getMeta('_business_hours');
		if( meta.isEmpty )
			return null;
		var data = json.decode(meta);
		return data;
	}
	String getExcerpt()
	{
		return '';
	}
}
