class MB_Category
{
	int		category_id;
	String	name;
	String	description;
	int		parent;
	int		store_id;
	int		image_id;
	String	image_url;
	
	MB_Category({this.category_id, this.name, this.description, this.parent, this.store_id, this.image_id});
	MB_Category.fromJson(Map<String, dynamic> data)
	{
		this.category_id = data['category_id'];
		this.name			= data['name'];
		this.description	= data['description'];
		this.parent			= data['parent'];
		this.store_id		= data['store_id'];
		this.image_id		= data['store_id'];
		this.image_url		= data['image_url'];
	}
}