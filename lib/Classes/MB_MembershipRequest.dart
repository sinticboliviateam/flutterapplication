import 'MB_Store.dart';

class MB_MembershipRequest
{
	String		firstname;
	String		lastname;
	String		email;
	String		phone;
	MB_Store	store;
	
	MB_MembershipRequest({this.firstname, this.lastname, this.email, this.phone, this.store});
	
	Map<String, dynamic> toMap()
	{
		return {
			'firstname': this.firstname,
			'lastname': this.lastname,
			'email': this.email,
			'phone': this.phone,
			'store': this.store != null ? this.store.toMap() : null
		};
	}
}
