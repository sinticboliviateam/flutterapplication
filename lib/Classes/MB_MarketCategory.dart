import 'SB_Object.dart';

class MB_MarketCategory extends SB_Object
{
	int		id;
	String	name;
	String	description;
	String	slug;
	int		image_id;
	
	MB_MarketCategory();
	
	MB_MarketCategory.fromMap(Map<dynamic, dynamic> data)
	{
		this.loadData(data);
	}
	@override
	void loadData(Map<dynamic, dynamic> data)
	{
		super.loadData(data);
		this.id				= data['id'];
		this.name			= data['name'];
		this.description	= data['description'];
		this.slug			= data['slug'];
		this.image_id		= data['image_id'];
	}
}
