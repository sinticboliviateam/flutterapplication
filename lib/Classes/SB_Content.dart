class SB_Content
{
	int		content_id;
	String	slug;
	String	title;
	String	content;
	String	excerpt;
	String	status;
	String	type;
	String	publish_date;
	String	end_date;
	String	url;
	List	images = [];
	String	creation_date;
	String	thumbnail_url;
	
	SB_Content()
	{
		
	}
	SB_Content.fromJson(Map<String, dynamic> data)
	{
		this.loadFromJson(data);
	}
	void loadFromJson(Map<String, dynamic> data)
	{
		this.content_id 	= data['content_id'];
		this.title			= data['title'];
		this.content		= data['content'];
		this.excerpt		= data['excerpt'];
		this.slug			= data['slug'];
		this.thumbnail_url	= data['thumbnail_url'];
		this.url			= data['url'];
		this.images			= data['images'];
	}
	String getTitle()
	{
		return this.title;	
	}
	String getExcerpt([int length = 150])
	{
		if( this.excerpt.length <= length )
			return this.excerpt;
		return this.excerpt.substring(0, length) + '...';
	}
}