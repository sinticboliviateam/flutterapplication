class MB_Comment
{
	int		id;
	int		object_id;
	String	object_type;
	int		author_id;
	int		parent_id;
	String	fullname;
	String	email;
	String	website;
	String	comment;
	int		nps;
	String	status;
	String	ip_address;
	String	user_agent;
	String	creation_date_gmt;
	String	creation_date;
	String	avatar_url;
	
	MB_Comment();
	factory MB_Comment.fromJson(Map<String, dynamic> data)
	{
		var comment = MB_Comment();
		comment.loadData(data);
		
		return comment;
	}
	void loadData(Map<String, dynamic> data)
	{
		this.id 			= data['id'];
		this.object_id 		= data['object_id'];
		this.object_type	= data['object_type'];
		this.author_id		= data['author_id'];
		this.parent_id		= data['parent_id'];
		this.fullname		= data['fullname'];
		this.email			= data['email'];
		this.website		= data['website'];
		this.comment		= data['comment'];
		this.nps				= data['nps'];
		this.status				= data['status'];
		this.ip_address			= data['ip_address'];
		this.user_agent			= data['user_agent'];
		this.creation_date_gmt	= data['creation_date_gmt'];
		this.creation_date		= data['creation_date'];
		this.avatar_url			= data['avatar_url'] != null ? data['avatar_url'] : null;
	}
	Map<String, dynamic> toMap()
	{
		return {
			'id': this.id,
			'object_id': this.object_id,
			'object_type': this.object_type,
			'author_id':	this.author_id,
			'parent_id':	this.parent_id,
			'fullname':		this.fullname,
			'email':		this.email,
			'website':		this.website,
			'comment':		this.comment,
			'nps':			this.nps,
			'status':		this.status,
			'ip_address':	this.ip_address,
			'user_agent':	this.user_agent,
		};
	}
}