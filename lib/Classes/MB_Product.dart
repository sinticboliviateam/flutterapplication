import 'MB_Attribute.dart';
import 'package:html/parser.dart';
import 'SB_Object.dart';

class MB_Product extends SB_Object
{
	int		product_id;
	String	product_name;
	String	product_description;
	String	slug;
	double	product_price;
	double	product_price_2;
	int		product_quantity;
	String	thumbnail;
	int		product_unit_measure;
	Map		measureUnit;
	int		store_id;
	List					attributes 	=	 [];
	String	link;
	
	String get name
	{
		return this.product_name;
	}
	String get description
	{
		return this.product_description;
	}
	double get price
	{
		return this.product_price;
	}
	String get permalink
	{
		return this.link;
	}
	MB_Product({this.product_id, this.product_name, this.product_description, this.slug, this.product_price, this.thumbnail});
	//MB_Product();
	MB_Product.fromJson(Map<dynamic, dynamic> json)
	{
		if( json != null )
			this.loadData(json);
	}
	MB_Product.fromMap(Map<dynamic, dynamic> data)
	{
		if( data != null )
			this.loadData(data);
	}
	void loadData(Map<dynamic, dynamic> data)
	{
		super.loadData(data);
		
		this.product_id 			= data['product_id'];
		this.product_name 			= data['product_name'];
		this.product_description 	= data['product_description'];
		this.slug 					= data['slug'];
		this.product_price			= data['product_price'].toDouble();
		this.product_price_2		= data['product_price_2'].toDouble();
		this.product_quantity		= data['product_quantity'].toInt();
		this.thumbnail				= data['thumbnailUrl'];
		this.product_unit_measure	= data['product_unit_measure'];
		this.measureUnit			= data['measureUnit'];
		this.link					= data['link'] ?? '';
		this.store_id				= data['store_id'] ?? 0;
		if( data['attributes'] != null )
		{
			this.attributes				= [];
			(data['attributes'] as List).forEach((attr)
			{
				this.attributes.add(new MB_Attribute.fromJson(attr));
			});
		}
	}
	String getImageUrl()
	{
		return "";
	}
	String getThumbnailUrl()
	{
		return this.thumbnail;
	}
	String getPrice()
	{
		return this.product_price.toStringAsFixed(2) + ' Bs';
	}
	String getPrice2()
	{
		return this.product_price_2.toStringAsFixed(2) + ' Bs';
	}
	MB_Attribute getAttributeById(int id)
	{
		MB_Attribute _attr = null;
		this.attributes.forEach((attr)
		{
			if( attr.id == id )
			{
				_attr = attr;
			}
		});
		
		return _attr;
	}
	/// Get product measure unit
	String getMu()
	{
		if( this.product_unit_measure == null || this.product_unit_measure <= 0 )
			return 'Unid';
			
		return this.measureUnit['code'];
	}
	String getName([int length = -1])
	{
		if( length == -1 )
			return this.product_name;
		
		if( this.product_name.length < length )
			return this.product_name;
			
		return this.product_name.substring(0, length) + '...';
	}
}
