import 'dart:convert';
import 'Service.dart';
import '../Classes/MB_Store.dart';
import '../Classes/MB_MembershipRequest.dart';
import '../Classes/MB_MarketCategory.dart';

class ServiceMarket extends Service
{
	
	static final ServiceMarket		_instance = ServiceMarket._constructor();
	
	ServiceMarket._constructor() : super()
	{
	}
	
	factory ServiceMarket() => _instance;
	
	Future<List<Map>> search(String keyword, [int page = 1, int limit = 20]) async
	{
		String _json 	= await Service.get('/monobusiness/market/search?keyword=$keyword&page=$page&limit=$limit');
		var obj 		= json.decode(_json);
		print(obj['data'].runtimeType);
		print( obj['data'].map<Map>( (i) => i as Map<String, dynamic>).toList() );
		return obj['data'].map<Map>( (i) => i as Map<String, dynamic>).toList();
		//return obj['data'];
	}
	Future<MB_Store> sendMembershipRequest(MB_MembershipRequest request) async
	{
		String _json = await Service.post('/monobusiness/market/membershiprequest', request.toMap());
		print(_json);
		var obj = json.decode(_json);
		
		return MB_Store.fromMap(obj['data']);
	}
	Future<List> getCities() async
	{
		String _json = await Service.get('/monobusiness/market/cities');
		print(_json);
		var obj = json.decode(_json);
		return obj['data'];
	}
	Future<List> getCategories() async
	{
		String _json = await Service.get('/monobusiness/market/categories');
		var obj = json.decode(_json);
		return obj['data'].map((c) => MB_MarketCategory.fromMap(c)).toList();
	}
	Future<void> driverAcceptOrder(int orderId) async
	{
		String _json = await Service.post('/monobusiness/market/drivers/orders/$orderId/accept', {});
		
	}
}
