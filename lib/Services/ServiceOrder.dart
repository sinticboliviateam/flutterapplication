import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../Classes/MB_Order.dart';
import 'Service.dart';

class ExceptionOrder implements Exception
{
	Map<String, dynamic>	response;
	String _error;
	
	ExceptionOrder(String error, Map response)
	{
		this._error = error;
		this.response = response;
	}
	String erroMsg()
	{
		return this._error;
	}
	Map<String, dynamic> getData() => this.response;
	String getError() => this.response['error'];
	int getCode() => this.response['code'];
}

class ServiceOrder extends Service
{
	static Future<MB_Order>	send(MB_Order order) async
	{
		String endpoint = Service.apiUrl + '/monobusiness/orders';
		print('Sending your order');
		print(endpoint);
		var orderData = order.toMap();
		orderData['use_user_customer'] = 1;
		var _json = json.encode(orderData);
		print(_json);
		print('===========');
		var headers		= await Service.getHeaders();
		print('header');print(headers);
		var response 	= await http.post(endpoint, headers: headers, body: _json);
		print(response.body);
		var obj 		= json.decode(response.body);
		if( response.statusCode != 200 )
			throw new ExceptionOrder('Error sending', obj);
		
		return MB_Order.fromJson(obj['data']);
	}
	static Future<List> getOrders() async
	{
		String endpoint = Service.apiUrl + '/monobusiness/orders';
		var headers = await Service.getHeaders();
		var response = await http.get(endpoint, headers: headers);
		var obj = json.decode(response.body);
		if( response.statusCode != 200 )
			throw new Exception('Ocurrio un error al recuperar los pedidos');
		var orders = [];
		obj['data'].forEach((o) 
		{
			orders.add( MB_Order.fromJson(o) );
		});
		
		return orders;
	}
}
