import 'Service.dart';
import '../Classes/MB_User.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../Classes/SB_Settings.dart';
import '../Classes/Exceptions/ExceptionLogin.dart';
import 'dart:io';
//import 'package:google_sign_in/google_sign_in.dart';
//import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class ServiceUsers extends Service
{
	static	bool loggedIn = false;
	
	static Future<SB_User> register(Map<String, dynamic> user) async
	{
		var endpoint = Service.apiUrl + '/v1.0.0/users/';
		var headers = {
			'Content-type': 'application/json'
		};
		String data = json.encode(user);
		var response = await http.post(endpoint, headers: headers, body: data);
			/*
			.then( (response)
			{
				print("Response status: ${response.statusCode}");
				print("Response body: ${response.contentLength}");
				print(response.headers);
				print(response.request);
			} );
			*/
		//print("Response status: ${response.statusCode}");
		//print("Response body: ${response.contentLength}");
		//print(response.headers);
		//print(response.request);
		print(response.body);
		var _json = json.decode(response.body);
		if( response.statusCode != 200 )
			throw new ExceptionRegister('Error de registro', response);
			
		var newUser = SB_User.fromJson(_json['data']);
		
		return newUser;
	}
	static Future<SB_User> login(String username, String password) async
	{
		String endpoint = Service.apiUrl + '/v1.0.0/users/get-token';
		var headers = {
			'Content-type': 'application/json'
		};
		String data = json.encode({'username': username, 'password': password});
		var response = await http.post(endpoint, headers: headers, body: data);
		var _json = json.decode(response.body);
		if( response.statusCode != 200 )
		{
			throw new ExceptionLogin('Error de atenticación', response);
		}
		var user = new SB_User.fromJson(_json['data']['user']);
		print("USER TOKEN: ${_json['data']['token']}");
		startSession(user, _json['data']['token']);
		return user;
	}
	static Future<SB_User> fbLogin(Map<String, dynamic> fb_data) async
	{
		String endpoint = Service.apiUrl + '/socialogin/fblogin';
		String data = json.encode(fb_data);
		var res = await http.post(endpoint, body: data, headers: {'Content-type': 'application/json'});
		var _json = json.decode(res.body);
		if( res.statusCode != 200 )
		{
			throw new ExceptionLogin('Error de atenticación', _json);
		}
		var user = new SB_User.fromJson(_json['data']['user']);
		startSession(user, _json['data']['token']);
		SB_Settings.saveString('fb_token', fb_data['token']);
		return user;
	}
	static Future<SB_User> googleLogin(Map<String, dynamic> gdata) async
	{
		String endpoint = Service.apiUrl + '/socialogin/googlelogin';
		String data = json.encode(gdata);
		print(endpoint);
		var res = await http.post(endpoint, body: data, headers: {'Content-type': 'application/json'});
		print(res.body);
		var _json = json.decode(res.body);
		if( res.statusCode != 200 )
		{
			throw new ExceptionLogin('Error de atenticación', (_json is Map) ? _json : res.body);
		}
		var user = new SB_User.fromJson(_json['data']['user']);
		startSession(user, _json['data']['token']);
		SB_Settings.saveString('google_token', gdata['token']);
		return user;
	}
	static Future<SB_User> appleLogin(Map<String, dynamic> adata) async
	{
		String endpoint = Service.apiUrl + '/socialogin/applelogin';
		String data		= json.encode(adata);
		var res			= await http.post(endpoint, body: data, headers: {'Content-type': 'application/json'});
		var obj			= json.decode(res.body);
		if( res.statusCode != 200 )
			throw new Exception('Error de autenticación, ${obj['error']}');
		var user = new SB_User.fromJson(obj['data']['user']);
		startSession(user, obj['data']['token']);
		SB_Settings.saveString('apple_token', adata['token']);
		return user;	
	}
	static Future<bool> isLoggedIn() async
	{
		bool authenticated = await SB_Settings.getBool('authenticated');
		if( authenticated == null )
			authenticated = false;
		return authenticated;
	}
	static void startSession(SB_User user, String token)
	{
		//##save token
		SB_Settings.saveString('token', token);
		SB_Settings.saveObject('user', user.toMap());
		SB_Settings.setBool('authenticated', true);
	}
	static void closeSession() async
	{
		await SB_Settings.saveString('token', '');
		await SB_Settings.saveString('user', '');
		await SB_Settings.setBool('authenticated', false);
		String gt = await SB_Settings.getString('google_token');
		String fbt = await SB_Settings.getString('fb_token');
		if( gt != null && !gt.isEmpty )
		{
			print('CLOSING GOOGLE SESSION');
			/*
			GoogleSignIn gsi = GoogleSignIn(scopes:['email', 'https://www.googleapis.com/auth/contacts.readonly']);
			//await gsi.disconnect();
			await gsi.signOut();
			*/
			await SB_Settings.saveString('google_token', null);
		}
		if( fbt != null && !fbt.isEmpty )
		{
			await SB_Settings.saveString('fb_token', null);
			/*
			final facebookLogin = FacebookLogin();
			await facebookLogin.logOut();
			*/
		}
	}
	static Future<SB_User> getCurrentUser() async
	{
		var data = await SB_Settings.getObject('user');
		if( data == null )
			return null;
		var user = SB_User.fromJson(data);
		
		return user;
	}
	static Future<SB_User> update(SB_User user) async
	{
		String endpoint = Service.apiUrl + '/v1.0.0/users/${user.user_id}';
		var headers = await Service.getHeaders();
		headers['Content-type'] = 'application/json';
		String data = json.encode(user.toMap());
		var res = await http.put(endpoint, headers: headers, body: data);
		print(res.body);
		var obj = json.decode(res.body);
		if( res.statusCode != 200 )
			throw new Exception('Ocurrion un error al tratar de actualizar los datos de usuario');
		//var user = new MB_User.fromJson(_json['data']);
		//return user;
	}
	static Future<Map<String, dynamic>> uploadAvatar(File avatar) async
	{
		String endpoint = Service.apiUrl + '/users/avatar';
		var headers = await Service.getHeaders();
		var request = http.MultipartRequest('POST', Uri.parse(endpoint));
		var pic 	= await http.MultipartFile.fromPath('qqfile', avatar.path);
		request.files.add(pic);
		headers.forEach( (key, value) 
		{
			request.headers[key] = value;
		});
		var response 	= await request.send();
		var data 		= await response.stream.toBytes();
		var _json 		= String.fromCharCodes(data);
		print(_json);
		
		return json.decode(_json);
	}
	static Future<SB_User> profile() async
	{
		String endpoint = Service.apiUrl + '/users/profile';
		var headers 	= await Service.getHeaders();
		var res 		= await http.get(endpoint, headers: headers);
		var _json 		= json.decode(res.body);
		
		if( res.statusCode != 200 )
			throw new Exception('Ocurrio un error al tratar de obtener la informacion del usuario');
		var user = new SB_User.fromJson(_json['data']);
		print('USER PROFILE');
		print(user.toMap());
		return user;	
	}
	static Future<void> reload() async
	{
		String token = await SB_Settings.getString('token');
		var user = await profile();
		startSession(user, token);
	}
	static Future<Map> recoverPass(String username) async
	{
		Map<String, String> data = {'username': username};
		String endpoint = Service.apiUrl + '/users/forgot';
		var headers 	= await Service.getHeaders();
		var res 		= await http.post(endpoint, body: json.encode(data), headers: headers);
		print(res.body);
		return json.decode(res.body);
	}
	static Future<Map> changePass(Map data) async
	{
		String endpoint = Service.apiUrl + '/users/pwd';
		var headers 	= await Service.getHeaders();
		var res 		= await http.post(endpoint, body: json.encode(data), headers: headers);
		print(res.body);
		return json.decode(res.body);
	}
	static Future<void> updateMetas(int id, Map<String, dynamic> data) async
	{
		String _json 	= await Service.put('/users/$id/metas', data);
		var obj			= json.decode(_json);

	}
}
