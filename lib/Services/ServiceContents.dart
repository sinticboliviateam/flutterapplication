import 'Service.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../Classes/SB_Content.dart';

class ServiceContents
{
	static Future<List> getContents(String type, [int limit = 10]) async
	{
		var endpoint = Service.apiUrl + '/contents/?type=${type}&limit=' + limit.toString();
		var res = await http.get(endpoint);
		var _json = json.decode(res.body);
		if( res.statusCode != 200 )
			throw new Exception(_json['error'] != null ? _json['error'] : 'Ocurrio un error al recuperar los contenidos');
		
		return _json['data'];
	}
	static Future<SB_Content> getContent(int id) async
	{
		String endpoint = Service.apiUrl + '/contents/' + id.toString();
		var res = await http.get(endpoint);
		var _json = json.decode(res.body);
		if( res.statusCode != 200 )
			throw new Exception(_json['error'] != null ? _json['error'] : 'Ocurrio un error al recuperar los contenidos');
			
		var content = SB_Content.fromJson(_json['data']);
		return content;
	}
	/// Get tours
	static Future<List> getHomePages([int limit = 5]) async
	{
		String endpoint = Service.apiUrl + '/peludos/home/pages';
		var res 		= await http.get(endpoint);
		var _json 		= json.decode(res.body);
		if( res.statusCode != 200 )
			throw new Exception(_json['error'] != null ? _json['error'] : 'Ocurrio un error al recuperar los contenidos');
		
		var data = _json['data'];
		List contents = <SB_Content>[];
		data.forEach((obj)
		{
			contents.add(SB_Content.fromJson(obj));
		});
		return contents;
	}
}
