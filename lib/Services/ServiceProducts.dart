import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../Classes/MB_Product.dart';
import '../Classes/MB_Category.dart';
import '../Classes/MB_Comment.dart';
import '../Classes/MB_Store.dart';
import 'Service.dart';

class ServiceProducts extends Service
{
	static ServiceProducts	_instance = ServiceProducts._constructor();
	
	ServiceProducts._constructor();
	
	factory ServiceProducts() => _instance;
	
	static Future<MB_Product> getProduct(int id) async
	{
		String _json 	= await Service.get('/monobusiness/products/$id');
		var obj			= json.decode(_json);
		
		return MB_Product.fromMap(obj['data']);
	}
	static Future<List> getIn(List ids) async
	{
		if( ids.length <= 0 )
			return <MB_Product>[];
		String endpoint = '/monobusiness/products/?limit=-1&ids=' + ids.join(',');
		String _json 	= await Service.get(endpoint);
		var obj 		= json.decode(_json);
		
		var products = new List<MB_Product>();
		return obj['data'].map<MB_Product>( (obj) => MB_Product.fromJson(obj) ).toList();
		
	}
	/**
	 * Get products from store
	 * 
	 */
	static Future<List> getProducts([int storeId = 0]) async
	{
		String endpoint = Service.apiUrl + '/monobusiness/products/latest/?limit=20';
		if( storeId > 0 )
			endpoint += '&store_id=$storeId';
			
		final response = await http.get(endpoint);
		if( response.statusCode != 200 )
			throw new Exception('Error obteniendo los productos');
		
		var res = json.decode(response.body);
		
		var products = new List<MB_Product>();
		res['data'].forEach( (obj) => products.add( MB_Product.fromJson(obj) ) );
		//print('Productos => ');
		//print(products);
		return products;
	}
	static Future<List> getLatest([int limit = 10]) async
	{
		String endpoint = Service.apiUrl + '/monobusiness/products/latest/?limit=' + limit.toString();
		final response = await http.get(endpoint);
		if( response.statusCode != 200 )
			throw new Exception('Error obteniendo los productos');
		
		var res = json.decode(response.body);
		
		var products = new List<MB_Product>();
		res['data'].forEach( (obj) => products.add( MB_Product.fromJson(obj) ) );
		//print('Productos => ');
		//print(products);
		return products;
	}
	static Future<List> search(String keyword) async
	{
		String endpoint = Service.apiUrl + '/monobusiness/products/search?' +
							'keyword=' + keyword;
		//print(endpoint);
		final response = await http.get(endpoint);
		if( response.statusCode != 200 )
			throw new Exception('Error en la busqueda de productos');
		var res = json.decode(response.body);
		var products = _parseProducts(res['data']);
		
		return products;
	}
	static Future<List> getCategoryProducts(int categoryId, [int limit = 10, int page = 1]) async
	{
		String endpoint = Service.apiUrl + '/monobusiness/categories/${categoryId}/products?limit=${limit}&page=${page}';
		final response = await http.get(endpoint);
		if( response.statusCode != 200 )
		{
			throw new Exception('Error al obtener los productos de la categoria');
		}
		var res = json.decode(response.body);
		var products = _parseProducts(res['data']);
		return products;
	}
	static List<MB_Product> _parseProducts(List data)
	{
		var products = new List<MB_Product>();
		data.forEach( (obj) 
		{
			products.add( MB_Product.fromJson(obj) );
		});
		
		return products;
	}
	static Future<List> getCategories([int parent = 0]) async
	{
		String endpoint = Service.apiUrl + '/monobusiness/categories';
		if( parent > 0 )
			endpoint += '?parent=' + parent.toString();
		final response = await http.get(endpoint);
		var res = json.decode(response.body);
		if( response.statusCode != 200 )
			throw new Exception('Error obteniendo las categories');
		
		var categories = new List<MB_Category>();
		res['data'].forEach( (obj) => categories.add( MB_Category.fromJson(obj) ) );
		//print('Productos => ');
		//print(products);
		return categories;
	}
	static Future<MB_Comment> sendComment(MB_Comment comment) async
	{
		String endpoint = Service.apiUrl + '/emono/comments';
		String data		= json.encode(comment.toMap());
		var headers		= await Service.getHeaders();
		var response	= await http.post(endpoint, body: data, headers: headers);
		var obj			= json.decode(response.body);
		if( response.statusCode != 200 )
		{
			throw new Exception("Ocurrio un error al enviar el comentario. \n ${obj['error']}");
		}
		
		var newComment = MB_Comment.fromJson(obj['data']);
		return newComment;
	}
	static Future<List> getComments(MB_Product product) async
	{
		String endpoint = Service.apiUrl + '/emono/products/' + product.product_id.toString() + '/comments?order=asc';
		var response 	= await http.get(endpoint);
		var obj			= json.decode(response.body);
		if( response.statusCode != 200 )
		{
			throw new Exception('Ocurrio un error al recuperar los comentarios.');
		}
		var comments = <MB_Comment>[];
		obj['data'].forEach((c)
		{
			var comment = MB_Comment.fromJson(c);
			comments.add(comment);
		});
		
		return comments;
	}
	static Future<List> getRelated(MB_Product product) async
	{
		String endpoint = Service.apiUrl + '/emono/products/' + product.product_id.toString() + '/related';
		var response = await http.get(endpoint);
		var obj			= json.decode(response.body);
		if( response.statusCode != 200 )
			throw new Exception('Ocurrio un error al recuperar los productos relacionados');
			
		return _parseProducts(obj['data']);
	}
	static Future<List> getMarketCategories() async
	{
		String endpoint = Service.apiUrl + '/monobusiness/market/categories';
		var response = await http.get(endpoint);
		var obj = json.decode(response.body);	
		if( response.statusCode != 200 )
			throw new Exception('Ocurrio un error al recuperar las categorias del market place');
			
		return obj['data'];
	}
	static Future<List> getMarketCategoryStores(int id) async
	{
		String endpoint = Service.apiUrl + '/monobusiness/market/categories/$id/stores';
		var response = await http.get(endpoint);
		var obj = json.decode(response.body);	
		if( response.statusCode != 200 )
			throw new Exception('Ocurrio un error al recuperar las categorias del market place');
		var stores = <MB_Store>[];
		obj['data'].forEach( (s) 
		{
			var store = MB_Store.fromMap(s);
			stores.add(store);
		});
		return stores;
	}
	static Future<Map> addFavorite(int id) async
	{
		String _json = await Service.get('/emono/favorites/product/$id');
		
		return json.decode(_json);
	}
	static Future<Map> removeFavorite(int id) async
	{
		String _json = await Service.delete('/emono/favorites/product/$id');
		
		var obj = json.decode(_json);
		return obj['data'];
	}
	static Future<List> getFavorites() async
	{
		String _json = await Service.get('/emono/favorites');
		
		var obj = json.decode(_json);
		
		return obj['data'];
	}
}
