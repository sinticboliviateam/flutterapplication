import 'dart:convert';
import 'Service.dart';
import '../Classes/MB_Store.dart';

class ServiceStores
{
	static final ServiceStores	_instance = ServiceStores._constructor();
	
	ServiceStores._constructor()
	{
		
	}
	factory ServiceStores() => _instance;
	
	Future<List<MB_Store>> getStoresIn(List ids) async
	{
		String _json = await Service.get('/monobusiness/stores?ids=' + ids.join(','));
		var obj = json.decode(_json);
		
		return obj['data'].map<MB_Store>( (s) => MB_Store.fromMap(s) ).toList();
	}
	Future<MB_Store> getStore(int id) async
	{
		String _json = await Service.get('/monobusiness/stores/$id');
		var obj = json.decode(_json);
		
		return MB_Store.fromMap(obj['data']);
	}
	Future<List> addFavorite(int id) async
	{
		String _json = await Service.post('/emono/favorites/store/$id', {});
		
		var obj = json.decode(_json);
		
		return obj['data'];
	}
	Future<List> removeFavorite(int id) async
	{
		String _json = await Service.delete('/emono/favorites/store/$id');
		
		var obj = json.decode(_json);
		
		return obj['data'];
	}
	Future<List> getFavorites() async
	{
		String _json = await Service.get('/emono/favorites/store');
		var obj = json.decode(_json);
		
		return obj['data'];
	}
}
