import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import '../Classes/SB_Settings.dart';
import '../Classes/Exceptions/RequestException.dart';

class Service
{
	//static String siteUrl = 'http://192.168.1.11/SBFramework';
	//static String apiUrl = 'http://192.168.1.11/SBFramework/api';
	static String siteUrl = 'https://aprovechalo.hexadatos.net';
	static String apiUrl = 'https://aprovechalo.hexadatos.net/api';
	
	/// Get all necessary headers to send a request
	static Future<Map<String, String>> getHeaders() async
	{
		Map<String, String> headers = {};
		String token = await SB_Settings.getString('token');
		if( token != null )
			headers['Authorization'] = 'Bearer ${token}';
			
		return headers;
	}
	static Future<Map<String, dynamic>> getSettings() async
	{
		String endpoint = Service.apiUrl + '/timbo/settings';
		var res 		= await http.get(endpoint);
		var _json 		= json.decode(res.body);
		if( res.statusCode != 200 )
			throw new Exception(_json['error'] != null ? _json['error'] : 'Ocurrio un error al recuperar las configuraciones de la aplicación');
		
		var data = _json['data'];
		
		return data;
	}
	static Future<String> get(String _endpoint) async
	{
		String endpoint 	= apiUrl + _endpoint;
		print('GET: $endpoint');
		var headers 		= await getHeaders();
		//print(headers);
		http.Response res 	= await http.get(endpoint, headers: headers);
		//var obj = json.decode(res.body);
		print('StatusCode => ${res.statusCode}');
		if( res.statusCode != 200 )
		{
			throw new RequestException('Ocurrio un error al tratar de recuperar los datos (GET)', res);
		}	
		
		return res.body;
	}
	static Future<String> post(String endpoint, Map<dynamic, dynamic> data) async
	{
		String url		= apiUrl + endpoint;
		
		var headers		= await getHeaders();
		var _json 		= json.encode(data);
		var response 	= await http.post(url, headers: headers, body: _json);
		print(_json);
		print(response.body);
		if( response.statusCode != 200 )
			throw new RequestException('Ocurrio un error al enviar la solicitud (POST)', response);
			
		return response.body;
	}
	static Future<String> put(String endpoint, Map<dynamic, dynamic> data) async
	{
		var headers		= await getHeaders();
		print('headers');print(headers);
		String _json	= json.encode(data);
		var response 	= await http.put(apiUrl + endpoint, headers: headers, body: _json);
		if( response.statusCode != 200 )
			throw new RequestException('Error en la solicitud PUT', response);
			
		return response.body;
	}
	static Future<String> delete(String endpoint, [Map data = null]) async
	{
		var headers		= await getHeaders();
		print('HEADERS: $headers');
		var response 	= await http.delete(apiUrl + endpoint, headers: headers);
		print(response.body);
		if( response.statusCode != 200 )
			throw new RequestException('Error en la solicitud PUT', response);
			
		return response.body;
	}
}
