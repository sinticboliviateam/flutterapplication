import 'package:flutter/material.dart';

//
final mainColors = {
	'purpleHex': 0xff45549d,
	'purple':		Color(0xff45549d),
	'lightGreen': 	Color(0xff00c091),
	'red': 			Color(0xffe33926),
	'darkRed': 		Color(0xff781510),
	'orange': 		Color(0xffe4891c),
	
};


MaterialColor primaryColor = MaterialColor(mainColors['purpleHex'],
	<int, Color>{
		50: mainColors['purple'],
		100: mainColors['purple'],
		200: mainColors['purple'],
		300: mainColors['purple'],
		400: mainColors['purple'],
		500: mainColors['purple'],
		600: mainColors['purple'],
		700: mainColors['purple'],
		800: mainColors['purple'],
		900: mainColors['purple'],
	}
);
var buttonColor = MaterialColor(0xfff75428, {
	50: Color(0xfff75428),
	100: Color(0xfff75428),
	200: Color(0xfff75428),
	300: Color(0xfff75428),
	400: Color(0xfff75428),
	500: Color(0xfff75428),
	600: Color(0xfff75428),
	700: Color(0xfff75428),
	800: Color(0xfff75428),
	900: Color(0xfff75428),
});
