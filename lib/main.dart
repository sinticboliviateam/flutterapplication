import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'Widgets/WidgetSelectUserType.dart';
import 'Providers/SearchProvider.dart';
import 'Providers/CartProvider.dart';
import 'Helpers/GeoLocationHelper.dart';
import 'Classes/SB_Settings.dart';
import 'Services/ServiceUsers.dart';
import 'Services/ServiceStores.dart';
import 'Services/ServiceProducts.dart';
import 'Widgets/WidgetUserHome.dart';
import 'Providers/SessionProvider.dart';
import 'colors.dart' as appColors;

void main() async
{
	//WidgetsFlutterBinding.ensureInitialized();
	//await Firebase.initializeApp();
	//FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBgHandler);
	/*
	await _fcm.setForegroundNotificationPresentationOptions(
		alert: true,
		badge: true,
		sound: true,
	);
	_fcm.getToken().then((String token)
	{
		print('FIREBASE TOKEN: $token');
	});
	_fcm.onTokenRefresh.listen( (String token) 
	{
		print('FIREBASE TOKEN: Changed => $token');
	});
	*/
	runApp(
		MultiProvider(
			providers: [
				//ChangeNotifierProvider(create: (_) => CommonProvider()),
				ChangeNotifierProvider(create: (_) => SessionProvider()),
				ChangeNotifierProvider(create: (_) => SearchProvider()),
				ChangeNotifierProvider(create: (_) => CartProvider()),
			],
			child: MyApp()
		)
	);
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async 
{
	print('myBackgroundMessageHandler');print(message);
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }

  // Or do other work.
}

class MyApp extends StatelessWidget 
{
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) 
	{
		
    return MaterialApp(
      title: 'Aprovechalo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: appColors.primaryColor,
		buttonColor: appColors.buttonColor,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
		appBarTheme: AppBarTheme(
			iconTheme: IconThemeData(
				color: Colors.white,
			),
			actionsIconTheme: IconThemeData(
				color: Colors.white
			),
			foregroundColor: Colors.white,
			textTheme: TextTheme(
				headline6: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)
			)
		)
      ),
      home: MyHomePage(),
		debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget 
{
  MyHomePage();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> 
{
	SessionProvider	_provider;
	final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
	
	@override
	void initState()
	{
		super.initState();
		this._loadData();
	}
	void _loadData() async
	{
		this._firebaseMessaging.configure(
			onMessage: (Map<String, dynamic> message) async {
				print("onMessage: $message");
				
			},
			onLaunch: (Map<String, dynamic> message) async {
				print("onLaunch: $message");
			
			},
			onResume: (Map<String, dynamic> message) async {
				print("onResume: $message");
			
			},
			onBackgroundMessage: myBackgroundMessageHandler,
		);
		this._firebaseMessaging.requestNotificationPermissions(
			const IosNotificationSettings(
				sound: true, badge: true, alert: true, provisional: true
			)
		);
		this._firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
			print("Settings registered: $settings");
		});
		this._firebaseMessaging.getToken().then((String token) async {
			assert(token != null);
			print('FIREBASE TOKEN: $token');
			if( await ServiceUsers.isLoggedIn() )
			{
				var user = await ServiceUsers.getCurrentUser();
				ServiceUsers.updateMetas(user.user_id, {'_firebase_token': token});
			}
		});
			
		GeoLocationHelper.getLocation().then((coordinate) 
		{
			SB_Settings.saveObject('location', coordinate.toMap());	
		}).catchError( (e) 
		{
			
		});
		ServiceUsers.isLoggedIn().then( (res) async
		{
			if( res )
			{
				Provider.of<SessionProvider>(context, listen: false).authenticated = true;
				Navigator.pushAndRemoveUntil(
					this.context, 
					MaterialPageRoute(builder: (ctx) => WidgetUserHome()), 
					(_) => false
				);
			}
			
		});
		
	}
	@override
	Widget build(BuildContext context) 
	{
		this._provider = Provider.of<SessionProvider>(context, listen: false);
		return Scaffold(
			/*
			appBar: AppBar(
				title: Text('', style: TextStyle(color: Colors.white)),
				actions: <Widget>[
					IconButton(
						icon: Icon(Icons.arrow_forward, color: Colors.white,),
						onPressed: ()
						{
							Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetSelectUserType()) );
						},
					)
				],
			),
			*/
			
			body: Container(
				child: Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: [
						Container(
							color: Theme.of(context).primaryColor,
							padding: EdgeInsets.all(40),
							child: Center(
								child: Text("Aprovechalo", 
									style: TextStyle(
										color: Colors.white, 
										fontWeight: FontWeight.normal, 
										fontSize: 50
									), 
									textAlign: TextAlign.center,
								)
							)
						),
						Expanded(
							child: Image.asset('images/cero-desperdicio.jpg', fit: BoxFit.cover,),
						),
						Expanded(
							child: Container(
							color: Colors.white,
							padding: EdgeInsets.all(10),
							child: Center(
								child: Column(
									crossAxisAlignment: CrossAxisAlignment.stretch,
									children: [
										Text("Bienvenido", style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold, fontSize: 48), textAlign: TextAlign.center,),
										SizedBox(height: 10),
										Text("Descripción aqui", style: TextStyle(color: Theme.of(context).primaryColor, fontSize:25), textAlign: TextAlign.center,),
										SizedBox(height: 20),
										Container(
											color: Theme.of(context).buttonColor,
											child: InkWell(
												child: Container(
													padding: EdgeInsets.all(15),
													child: Text("Continuar", style: TextStyle(color: Colors.white), textAlign: TextAlign.center,) 
												),
												onTap: ()
												{
													Navigator.push(context, MaterialPageRoute(builder: (_) => WidgetSelectUserType()));
												},
											)
										),
									]
								)
							)
						)
						)
					]
				)
			),
		/*
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
		*/
    );
  }
}
