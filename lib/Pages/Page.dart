import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import '../Classes/SB_Content.dart';
import '../Services/ServiceContents.dart';

class PageContent extends StatelessWidget
{
	final	int	id;
	
	PageContent({@required this.id});
	
	@override
	Widget build(BuildContext context)
	{
		return FutureBuilder(
			future: ServiceContents.getContent(this.id),
			builder: (ctx, snapshot)
			{
				if( !snapshot.hasData )
					return Container(child: Center(child: CircularProgressIndicator()));
					
				var page = (snapshot.data as SB_Content);
				
				return Scaffold(
					appBar: AppBar(title: Text(page.title)),
					body: Container(
						padding: EdgeInsets.all(10),
						child: SingleChildScrollView(
							child: Html(
								data: page.content
							)
						)
					)
				);
			}
		);
	}
}
